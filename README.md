#  WhereAppU

* React Native App Utilizing [Ignite](https://github.com/infinitered/ignite)

## WhereAppU project

![WhereAppU project](https://lh3.googleusercontent.com/GzLFiaN3V3GKkcNSsC84ZWA7Aa5x3CWqUF1aeq4pjBuiWlpvWShQ9REKkcqFKLQkos7KrJ5DdZf3gw1iA03ASKw0Wy6W1n5YNnUUpy6m1JumScb9dj7ILzHVUPHAiW2x62rwqDRbNDf99hMJ83-XAnRkCzwHblDXjH5wNQ2iue3_XZ4P6WPvS4q_p4pCFzzBiSCVhoV4i9POCosMKDk6tIKDX9itlurTZMsfkpFJVcftL447abgBpTxNI3soxbb3qNnb0z2_lzvqTciO-1FN3CvQTdqDXpgaSv7YcP_hSSx1SWXD_HU44HYVVz_ZaPmmL9vZERWqRLpDzvdMqlFLp48Lj7DO4lM3tCNUZLzGl_615g2Ruyc_oj5sqgzjXNDtMOgmy1ecEyR_7oEvvnIRfy8zlIXMdDIywGxc8dhKNBl5vg2sBrr2gsvh0M8mvb5_rL_mA_K-5_VX5sSHEV5tI47ryBs--Ha4tWqE2BVq2X4e_HbK6AKzzMop9t1p-nWFVSVpcsWCR-v_zHUkmfplBaKicHF2aaYFiiA6NzLle9N3g8p8aCd7A8oVR0_52XOEJGYoX4S5auWx4GCVpzidFSvUdzb9i974=w1358-h605 "WhereAppU roadmap")

## :arrow_up: How to Setup

**Step 1:** git clone this repo and all submodules: `git clone --recurse-submodules -j8 https://gitlab.com/whereappu-team/WhereAppU.git`

**Step 2:** cd to the cloned repo:

**Step 3:** Install the Application with `yarn` or `npm i`

Check on .gitmodules to see other included repos  
To update all submodules at once: `git submodule update --recursive`  
If coming from older branch (submodules folders appear empty): `git submodule update --init --recursive`
Note: don't forget to commit changes if syncing submodules

## :arrow_forward: How to Run App

1. cd to the repo
2. Run Build for either OS
  * for iOS
    * run `react-native run-ios`
  * for Android
    * Run Genymotion
    * run `react-native run-android`

## :arrow_forward: Useful commands

1. `watchman watch-del-all`
2. `rm -rf node_modules && npm install`
3. `rm -rf ./node_modules/react-native-sms-x/node_modules`
4. `rm -rf /tmp/metro-bundler-cache-*`
5. `npm start -- --reset-cache`
6. `rm -rf /tmp/haste-map-react-native-packager-*`

* `adb reverse tcp:8081 tcp:8081`
* `adb shell input keyevent 82`
* localhost:8081/debugger-ui/

## App requirements

We currently support Android 6+ devices with Google Play Services enabled to allow better geolocation.  
In the future we expect to lower entry bar for older Android versions, and make it compatible for iOS.
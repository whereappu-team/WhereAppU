import { takeLatest, takeEvery, all } from 'redux-saga/effects';
import API from '../Services/Api';

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux';
import { SocketTypes } from '../Redux/SocketRedux';
import { SOSTypes } from '../Redux/SOSRedux';
import { GeneralAlertsTypes } from '../Redux/GeneralAlertsRedux';
import { ProcessedAlertsTypes } from '../Redux/ProcessedAlertsRedux';
import { ConnectionTypes } from '../Redux/ConnectionRedux';
import { PushNotificationTypes } from '../Redux/PushNotificationRedux';
import { UserDataTypes } from '../Redux/UserDataRedux';

/* ------------- Sagas ------------- */
import { startup } from './StartupSagas';
import { manageConectivity } from './ConnectionSagas';
import { receivePushNotification } from './PushNotificationSagas';
import {
  sendSOS,
  sendSync,
  sendSyncStatus,
  listenSOS,
  listenSync,
  listenSyncStatus,
  registerUser } from './SocketSagas';
import {
  sendSOSAlert,
  getGeneralAlerts,
  getProcessedAlerts
} from './AlertsSagas';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create();

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    takeLatest(StartupTypes.STARTUP, startup),
    takeEvery(SocketTypes.SEND_SOS, sendSOS),
    takeEvery(SocketTypes.SEND_SYNC, sendSync),
    takeEvery(SocketTypes.SEND_SYNC_STATUS, sendSyncStatus),
    takeLatest(SOSTypes.SOS_FETCH, sendSOSAlert, api),
    takeLatest(GeneralAlertsTypes.GENERAL_ALERTS_FETCH, getGeneralAlerts, api),
    takeLatest(ProcessedAlertsTypes.PROCESSED_ALERTS_FETCH, getProcessedAlerts, api),
    takeLatest(ConnectionTypes.CHECK_IF_CONNECTED, manageConectivity),
    takeLatest(SocketTypes.INIT_LISTENERS, listenSOS),
    takeLatest(SocketTypes.INIT_LISTENERS, listenSync),
    takeLatest(SocketTypes.INIT_LISTENERS, listenSyncStatus),
    takeLatest(PushNotificationTypes.NEW_PUSH_NOTIFICATION, receivePushNotification),
    takeLatest(UserDataTypes.GET_DEVICE_UNIQUE_ID, registerUser)
  ]);
}

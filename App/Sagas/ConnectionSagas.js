// Utils
import { call, put, select } from 'redux-saga/effects'
import { delay } from 'redux-saga'

// Libs
import wifi from 'react-native-android-wifi'

// Reducer Actions
import ModalsActions from '../Redux/UI/ModalsRedux'
import connectionActions from '../Redux/ConnectionRedux'

// Selectors
import { connectionSelectors } from '../Redux/ConnectionRedux'

function connectToTCP () {
	console.tron.log('pasarno 10 b')
	wifi.loadWifiList((wifiStringList) => {
	console.tron.log('pasarno 10 C', wifiStringList)
		const wifiArray = JSON.parse(wifiStringList)
		const wauWifi = wifiArray.find(wifi =>
			wifi.SSID === 'WhereAppU-WiFi')
		if (wauWifi) {
			wifi.findAndConnect(wauWifi.SSID, '', (found) => {
				if (found) {
					// yield put(ModalsActions.openConnectionModal('wifi'))
					console.tron.log("wauWifi is in range");
				} else {
					console.tron.log("wauWifi is NOT in range");
				}
			})
		} else {
			console.tron.log("elseelse wauWifi is NOT in range");
		}
	},
	(error) => {
		console.tron.log('error en la verga', error)
	})
}

export function* manageConectivity() {

	yield delay(5000)
	const connectionRedux = yield select(connectionSelectors.selectConnection)
	const { isConnected } = connectionRedux

	if (!isConnected) {
		yield put(ModalsActions.openConnectionModal('mesh'))
	}
}
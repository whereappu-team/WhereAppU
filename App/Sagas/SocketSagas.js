// Utils
import { call, put, select } from 'redux-saga/effects';
import Socket from '../Lib/SocketIO';

// Reducer Actions
import SocketActions from '../Redux/SocketRedux';
import StartupActions from '../Redux/StartupRedux';
import UserDataActions from '../Redux/UserDataRedux';

// Selectors
import { userDataSelectors } from '../Redux/UserDataRedux';

//Notifications
import { pushNotifications } from '../Notifications';

export function* sendSOS () {
  yield put(StartupActions.startup('user_action'));
  const sosObject = yield select(userDataSelectors.selectUserData);

  // @TODO:
  // 1. detect if user is either offline or online
  // 1.a. online
  //    1.a.1 => send via socket
  // 1.b. offline
  //    1.b.1 => is WhereAppU-WiFi available ? -> send via TcpClient()
  //    1.b.2 => is completely offline -> use mesh

  const response = yield call(Socket.sendSOS, sosObject);
  if (response instanceof Error) {
    yield put(SocketActions.failedSos(response));
    return;
  }

  yield put(SocketActions.sentSos(sosObject));
  yield call(Socket.trackSOS);
  yield put(SocketActions.broadcastedSos());
  yield put(UserDataActions.removeUserEmergency());
}

export function* listenSOS () {
  while (true) {
    console.tron.log('666 SOCKETIO', 'listening sos');
    const userRefs = yield call(Socket.listenSOS);
    console.tron.log(' * _ * SOS - RECIEVE - listenSOS', userRefs);
    pushNotifications.localUserNotification({
      'msg': `Hi, your friend ${userRefs.user_id} is requesting help, is under risk of ${userRefs.disaster_type}`,
      'location': userRefs.user_loc
    });
    yield put(SocketActions.receivedSos(userRefs));
  }
}

export function* sendSync () {
  const sosObject = yield select(userDataSelectors.selectUserData);

  console.tron.log('sosObject', sosObject);
  const { payload } = sosObject;
  const response = yield call(Socket.sendSync, payload);
  if (response instanceof Error) {
    yield put(SocketActions.failedSync(response));
    return;
  }
  yield put(SocketActions.sentSync(sosObject));
  yield call(Socket.trackSync);
  yield put(SocketActions.broadcastedSync());
}

export function* listenSync () {
  while (true) {
    console.tron.log('666 SOCKETIO', 'listenSync sos');

    const sync = yield call(Socket.listenSync);
    console.tron.log(' * _ * CHECKCONTACTS - RECIEVE - listenSync', sync);
    pushNotifications.localLisenSyncNotification({
      'msg': 'Hi, your friend is asking for your status, are you OK?'
    });
    yield put(SocketActions.receivedSync(sync));
  }
}

export function* sendSyncStatus (status) {
  const response = yield call(Socket.sendSyncStatus, status.payload);
  if (response instanceof Error) {
    yield put(SocketActions.failedSyncStatus(response));
    return;
  }
  yield put(SocketActions.sentSyncStatus(status));
  yield call(Socket.trackSyncSatus);
  yield put(SocketActions.transmittedSyncStatus());
}

export function* listenSyncStatus () {
  console.tron.log('666 SOCKETIO', 'listenSyncStatus sos');

  while (true) {
    const status = yield call(Socket.listenSyncStatus);
    yield put(SocketActions.receivedSyncStatus(status));
  }
}

const cclessPhoneNumber = phoneNumber => phoneNumber.replace(/^(?:\+\d{1,2}[.-\s]?)(.*)$/, '$1').replace(/[.-\s]/g, '');

export function* registerUser () {
  const userData = yield select(userDataSelectors.selectUserData);
  const userId = cclessPhoneNumber(userData.user_id);
  yield put(UserDataActions.parseUniqueId(userId));
  yield call(Socket.init, userId);

  console.tron.log('[SOCKETIO] registered user', userId);
}
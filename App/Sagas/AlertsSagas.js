// Utils
import { call, put } from 'redux-saga/effects';

// Libs
import Location from '../Lib/Location';
// import BackgroundTask from 'react-native-background-task';
// import { pushNotifications } from '../../Notifications';
import wifi from 'react-native-android-wifi';

// Reducer Actions
import SOSActions from '../Redux/SOSRedux';
import GeneralAlertsActions from '../Redux/GeneralAlertsRedux';
import ProcessedAlertsActions from '../Redux/ProcessedAlertsRedux';
import ModalsActions from '../Redux/UI/ModalsRedux';

import geolib from 'geolib';

export function* sendSOSAlert(api, action) {

  const { payload } = action;
  const response = yield call(api.sendSOSAlert, payload);

  if (response.ok) {
    yield put(SOSActions.sosSuccess(response));
    yield put(ModalsActions.notificationToggle(true));
  } else {
    yield put(SOSActions.sosFailure(response));
  }
}

export function* getGeneralAlerts(api, action) {
  const { userId } = action;
  const response = yield call(api.getGeneralAlerts, userId);

  if (response.ok) {
    yield put(GeneralAlertsActions.generalAlertsSuccess(response));
    yield put(ModalsActions.notificationToggle(true));
  } else {
    yield put(GeneralAlertsActions.generalAlertsFailure(response.error));
  }
}

export function* getProcessedAlerts(api) {
  const current_position = yield call(Location.getCurrentLocation);
  const response = yield call(api.getProcessedAlerts);

  if (response.ok) {
    yield put(ProcessedAlertsActions.processedAlertsSuccess(response));
    let temp_alerts = [];
    for (const alert of response.data.response) {
      if (reviewBB({
        lat: current_position.coords.latitude,
        lng: current_position.coords.longitude
      }, alert.BOUNDING_BOX)) {
        temp_alerts.push(alert);
      }
    }
    console.log('ALERTS', temp_alerts);
    yield put(ProcessedAlertsActions.updateMatchedAlerts(temp_alerts));
  } else {
    yield put(ProcessedAlertsActions.processedAlertsFailure(response.error));
  }
}

const reviewBB = (position, _data) => {
  let data_array = _data.substr(1, (_data.length-2)).split(',');
  //let data_array = [-73.995039,4.833039,-74.493561,3.675383];
  let data_object = [
    {latitude: data_array[1], longitude: data_array[0]},
    {latitude:data_array[1], longitude: data_array[2]},
    {latitude: data_array[3], longitude: data_array[2]},
    {latitude:data_array[3], longitude: data_array[0]}
  ];
  let status = geolib.isPointInside({ latitude: position.lat, longitude: position.lng }, data_object);
  return status;
};

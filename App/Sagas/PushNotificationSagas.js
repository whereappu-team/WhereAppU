// Utils
import { call, put, select } from 'redux-saga/effects';

//Libs
import { NavigationActions } from 'react-navigation';

// Reducer Actions
import ModalsActions from '../Redux/UI/ModalsRedux';
import SocketActions from '../Redux/SocketRedux';
import StartupActions from '../Redux/StartupRedux';

// Selectors
import { userDataSelectors } from '../Redux/UserDataRedux';


export function* receivePushNotification(action) {
  //console.tron.log('*** action *** ', action);
  const payload = JSON.parse(action.pushNotification.dataJSON);
  if (payload.action == 'View on Map') {
    console.tron.log('VIEW ON MAP');
		let from = parseInt(payload.message.match(/[0-9]+/)[0], 10);
		console.tron.log('Location',payload.location);
		let data = {
			  givenName: '',
			  middleName: '',
			  familyName: '',
			  phone: from.toString(),
				status: 'emergency',
				location: payload.mapData
			}
		yield put(NavigationActions.navigate({ routeName: 'ContactView', params:data }));
		
  } else if (payload.action == 'I´m OK') {
    let from = parseInt(payload.message.match(/[0-9]+/)[0], 10);
    yield put(StartupActions.startup('user_action'));
    const sosObject = yield select(userDataSelectors.selectUserData);
    console.tron.log('I´M OK', sosObject);
    yield put(SocketActions.sendSyncStatus({
      'user_from': from.toString(),
      'user_to': sosObject.user_id,
      'ok': true
    }));
  } else if (payload.action == 'I Need Support') {
    let from = parseInt(payload.message.match(/[0-9]+/)[0], 10);
    yield put(StartupActions.startup('user_action'));
    const sosObject = yield select(userDataSelectors.selectUserData);
    console.tron.log('I NEED SUPPORT', sosObject);
    yield put(SocketActions.sendSyncStatus({
      'user_from': from.toString(),
      'user_to': sosObject.user_id,
      'ok': false,
      'loc': {
        'lat': sosObject.user_loc.lat,
        'lng': sosObject.user_loc.lng
      }
    }));
    yield put(ModalsActions.selectEmergencyToggle());
  }
}
// Libs
import { fork, take, call, put, cancel, select } from 'redux-saga/effects';

// Custom Libs
import Socket from '../Lib/SocketIO';
import Location from '../Lib/Location';

// Selectors
import { contactsSelectors } from '../Redux/ContactsRedux';
import { userDataSelectors } from '../Redux/UserDataRedux';

// Actions
import UserDataActions from '../Redux/UserDataRedux';
import ModalsActions from '../Redux/UI/ModalsRedux';
import SocketActions from '../Redux/SocketRedux';

const cclessPhoneNumber = phoneNumber => phoneNumber.replace(/^(?:\+\d{1,2}[.-\s]?)(.*)$/, '$1').replace(/[.-\s]/g, '');

export function* startup (customAction) {

  const userData = yield select(userDataSelectors.selectUserData);
  const contacts = Object.values(yield select(contactsSelectors.selectContacts));

  if (contacts.length > 0) {
    const contactNumbers = contacts.map(contact => cclessPhoneNumber(contact.phoneNumbers[0].number));
    if (contactNumbers.length > 0) {
      yield put(UserDataActions.getSelectedContacts(contactNumbers));
    }
  }

  const location = yield call(Location.getCurrentLocation);
  if (location.coords) {
    const { longitude, latitude } = location.coords;
    yield put(UserDataActions.getUserLocation({
      lat: latitude,
      lng: longitude
    }));
  }

  const ok = yield call(Socket.init, userData.user_id);
  if (ok) {
    yield put(SocketActions.initListeners());
  }
}

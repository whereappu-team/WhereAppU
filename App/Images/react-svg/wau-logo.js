import React, { Component } from 'react';
import Svg,{
    G,
    Path,
    RadialGradient,
} from 'react-native-svg';

class wauLogo extends Component{
    render(){
        return (

            <Svg height="40" width="170" viewBox="0 0 189.2 40">
                <G>
                    <RadialGradient id="SVGID_1_" cx="31.37" cy="20.0307" r="4.0883" gradientTransform="matrix(3.6693 0 0 3.6693 -100.1036 -58.497)" gradientUnits="userSpaceOnUse">
                        <stop  offset="0" stopColor="#F16522"/>
                        <stop  offset="0.5" stopColor="#F16522"/>
                        <stop  offset="0.7543" stopColor="#F6921E"/>
                        <stop  offset="1" stopColor="#F16522"/>
                    </RadialGradient>
                    <Path fill="url(#SVGID_1_)" d="M2.1,22.7C4.8,27.1,9.5,30,15,30c5.5,0,10.2-2.9,12.9-7.3c1.3-2.8,2.1-5.6,2.1-7.7c0-8.3-6.7-15-15-15
                        C6.7,0,0,6.7,0,15C0,17.2,1.7,21.2,2.1,22.7z M15,6.9c4.5,0,8.1,3.6,8.1,8.1c0,4.4-3.6,8.1-8.1,8.1c-4.5,0-8.1-3.6-8.1-8.1
                        C6.9,10.5,10.5,6.9,15,6.9z M0,15c0,2.4,1.2,6,2.1,7.7C4.8,27.1,9.5,30,15,30"/>
                    <G>
                        <RadialGradient id="SVGID_2_" cx="31.3876" cy="22.1971" r="5.3094" gradientTransform="matrix(3.6693 0 0 3.6693 -100.1036 -58.497)" gradientUnits="userSpaceOnUse">
                            <stop  offset="0" stopColor="#F6921E"/>
                            <stop  offset="0.4297" stopColor="#F37C20"/>
                            <stop  offset="0.7058" stopColor="#F26F21"/>
                            <stop  offset="1" stopColor="#F16522"/>
                        </RadialGradient>
                        <Path fill="url(#SVGID_2_)" d="M15,30c-5.5,0-10.3-2.9-12.9-7.3C5.8,30.8,13.1,40,15,40c1.9,0,9.2-9.2,12.9-17.3C25.3,27.1,20.5,30,15,30z"/>
                    </G>
                    <Path fill="#FFFFFF" d="M18.3,7.6c1.2,0.6,2.3,1.4,3.1,2.4c1.2-2,3-3.7,5.2-4.5c-0.9-1.1-1.9-2.1-3.1-2.9C21.4,3.8,19.6,5.6,18.3,7.6z
                        "/>
                    <Path fill="#FFFFFF" d="M3.4,5.5c2.2,0.8,4.1,2.4,5.3,4.4c0.8-1,1.9-1.8,3.1-2.4c-1.3-2.1-3.1-3.8-5.3-4.9C5.3,3.4,4.3,4.4,3.4,5.5z"
                            />
                    <Path fill="#FFFFFF" d="M22.5,18.1c-0.5,1.2-1.3,2.3-2.4,3.1c2.5,1.5,4.3,4,4.9,7c0.9-1.5,1.8-3.1,2.5-4.7
                            C26.3,21.3,24.6,19.4,22.5,18.1z"/>
                    <Path fill="#FFFFFF" d="M9.9,21.2c-1-0.8-1.8-1.9-2.4-3.1c-2.1,1.3-3.9,3.2-5,5.5c0.7,1.5,1.6,3.1,2.5,4.7C5.6,25.2,7.4,22.7,9.9,21.2 z"/>
                </G>
                <G>
                    <Path fill="#FFFFFF" d="M63.3,8.9h3.3l-6.5,18.3h-2.7l-4.6-13.1l-4.6,13.2h-2.8L39,8.9h3.4L47,22.6l4.6-13.7H54l4.6,13.7L63.3,8.9z"/>
                    <Path fill="#FFFFFF" d="M79.9,15.5c0.7,0.8,1.1,2.1,1.1,3.9v7.8h-3.3v-7.7c0-0.9-0.2-1.6-0.6-2.1c-0.4-0.5-0.9-0.7-1.8-0.7
                        c-0.9,0-1.6,0.2-2.2,0.8s-0.8,1.4-0.8,2.4V27h-3.3V8.9h3.3v7.4c0.5-0.7,1.1-1.2,1.8-1.5c0.7-0.4,1.5-0.6,2.4-0.6
                        C78,14.2,79,14.7,79.9,15.5z"/>
                    <Path fill="#FFFFFF" d="M95.7,21.4h-8.6c0.1,1.2,0.5,2,1.1,2.6c0.6,0.6,1.4,0.8,2.6,0.8c1.4,0,2.7-0.5,3.9-1.3l0.9,2.2
                        c-0.6,0.5-1.3,0.8-2.2,1.2c-0.9,0.2-1.8,0.5-2.7,0.5c-2.1,0-3.8-0.6-4.9-1.8c-1.2-1.2-1.8-2.8-1.8-4.8c0-1.3,0.2-2.5,0.8-3.4
                        c0.5-1.1,1.3-1.8,2.2-2.4c0.9-0.6,2-0.8,3.2-0.8c1.8,0,3.2,0.6,4.1,1.6c1.1,1.2,1.5,2.7,1.5,4.6v0.9H95.7z M88,17.3
                        c-0.5,0.6-0.8,1.3-0.9,2.4h5.9c0-1.1-0.4-1.9-0.8-2.4c-0.5-0.6-1.2-0.8-2-0.8C89.2,16.4,88.5,16.8,88,17.3z"/>
                    <Path fill="#FFFFFF" d="M106.8,16.9l-1.9,0.2c-2.1,0.2-3.2,1.3-3.2,3.3v6.8h-3.3V14.6h3.1v2.2c0.7-1.5,2-2.4,4.1-2.5l0.9-0.1
                        L106.8,16.9z"/>
                    <Path fill="#FFFFFF" d="M119.6,21.4H111c0.1,1.2,0.5,2,1.1,2.6s1.4,0.8,2.6,0.8c1.4,0,2.7-0.5,3.9-1.3l0.9,2.2
                        c-0.6,0.5-1.3,0.8-2.2,1.2c-0.9,0.2-1.8,0.5-2.7,0.5c-2.1,0-3.8-0.6-4.9-1.8c-1.2-1.2-1.8-2.8-1.8-4.8c0-1.3,0.2-2.5,0.8-3.4
                        c0.5-1.1,1.3-1.8,2.2-2.4c0.9-0.6,2-0.8,3.2-0.8c1.8,0,3.2,0.6,4.1,1.6c1.1,1.2,1.5,2.7,1.5,4.6L119.6,21.4L119.6,21.4z
                        M111.9,17.3c-0.5,0.6-0.8,1.3-0.9,2.4h5.9c0-1.1-0.4-1.9-0.8-2.4c-0.5-0.6-1.2-0.8-2-0.8S112.5,16.8,111.9,17.3z"/>
                    <Path fill="#F7941D" d="M136.8,27.2l-1.9-4.2h-9l-1.9,4.2h-3.4l8.2-18.3h2.7l8.2,18.3H136.8z M127.1,20.3h6.7l-3.3-7.8L127.1,20.3z"/>
                    <Path fill="#F7941D" d="M152.2,15c0.8,0.6,1.5,1.3,2,2.4c0.5,1.1,0.7,2.1,0.7,3.5c0,1.3-0.2,2.5-0.7,3.4c-0.5,0.9-1.2,1.8-2,2.2
                        c-0.8,0.6-1.8,0.8-2.9,0.8c-0.9,0-1.6-0.2-2.4-0.6c-0.7-0.4-1.3-0.9-1.6-1.5v6.6H142V14.6h3.2v1.9c0.4-0.7,0.9-1.3,1.6-1.6
                        c0.7-0.4,1.5-0.6,2.5-0.6C150.4,14.2,151.4,14.4,152.2,15z M150.9,23.9c0.6-0.7,0.8-1.6,0.8-2.9c0-1.3-0.2-2.4-0.8-3.1
                        c-0.6-0.7-1.4-1.1-2.4-1.1c-1.1,0-1.9,0.4-2.4,1.1c-0.6,0.7-0.8,1.8-0.8,3.1s0.2,2.4,0.8,3.1c0.6,0.7,1.4,1.1,2.4,1.1
                        C149.5,24.9,150.3,24.6,150.9,23.9z"/>
                    <Path fill="#F7941D" d="M168.1,15c0.8,0.6,1.5,1.3,2,2.4c0.5,1.1,0.7,2.1,0.7,3.5c0,1.3-0.2,2.5-0.7,3.4c-0.5,0.9-1.2,1.8-2,2.2
                        c-0.8,0.6-1.8,0.8-2.9,0.8c-0.9,0-1.6-0.2-2.4-0.6c-0.7-0.4-1.3-0.9-1.6-1.5v6.6h-3.3V14.6h3.2v1.9c0.4-0.7,0.9-1.3,1.6-1.6
                        c0.7-0.4,1.5-0.6,2.5-0.6C166.2,14.2,167.3,14.4,168.1,15z M166.7,23.9c0.6-0.7,0.8-1.6,0.8-2.9c0-1.3-0.2-2.4-0.8-3.1
                        c-0.6-0.7-1.4-1.1-2.4-1.1c-1.1,0-1.9,0.4-2.4,1.1c-0.6,0.7-0.8,1.8-0.8,3.1s0.2,2.4,0.8,3.1c0.6,0.7,1.4,1.1,2.4,1.1
                        C165.3,24.9,166.1,24.6,166.7,23.9z"/>
                    <Path fill="#FFFFFF" d="M175.9,25.5c-1.3-1.3-2-3.2-2-5.7v-11h3.3V20c0,1.5,0.4,2.7,1.1,3.5c0.7,0.8,1.9,1.2,3.3,1.2s2.5-0.4,3.3-1.2
                        c0.7-0.8,1.1-2,1.1-3.5V8.9h3.3v10.8c0,2.5-0.7,4.4-2,5.7c-1.3,1.3-3.2,2-5.7,2S177.2,26.8,175.9,25.5z"/>
                </G>
            </Svg>
    
        )
    }
}

export default wauLogo;
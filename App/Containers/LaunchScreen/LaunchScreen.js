import React, { Component } from 'react';
import { Image, Text } from 'react-native';

// Libs
import Permissions from '../../Lib/Permissions';
import LinearGradient from 'react-native-linear-gradient';

// Styles
import { Colors } from '../../Themes';
import { Images } from '../../Themes';
import styles from './LaunchScreenStyles';

export default class LaunchScreen extends Component {
  componentDidMount() {
    Permissions.obtainPermissions([
      'SEND_SMS',
      'READ_CONTACTS',
      'ACCESS_FINE_LOCATION',
      'READ_PHONE_STATE'
    ]).then((hasPermissions) => {
      if (hasPermissions) {
        setTimeout(() => {
          this.props.navigation.navigate('DashboardView');
        }, 2000);
      }
    });

    console.ignoredYellowBox = ['Setting a timer'];
  }

  render() {
    return (
      <LinearGradient
        colors={[Colors.gradPrimary, Colors.gradSecondary, Colors.gradPrimary]}
        locations={[0,0.5,1]}
        style={styles.launchContainer}
      >
        <Image source={Images.logo} resizeMode="contain" />
        <Text style={styles.copyVersion}>Ver 1.0</Text>
      </LinearGradient>
    );
  }
}

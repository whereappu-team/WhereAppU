import { StyleSheet } from 'react-native'
import { Colors, ApplicationStyles } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  containerHeader: {
    flexDirection: 'row'
  },

  viewContact: {
      backgroundColor: Colors.orangePrimary,
      height: 40,
      justifyContent: 'center',
      fontSize: 20,
      color: Colors.white,
      textAlign: "center",
      width: "100%",
      paddingTop: 5
  },

  fieldContainer: {
    width: 300,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
    marginHorizontal: 15,
    marginTop: 5
  },
  labelField: {
    color: Colors.orangeSecundary,
    marginVertical: 10
  },
  labelDataField: {
    color: Colors.white,
    marginVertical: 10,
    fontSize: 15
  },
});

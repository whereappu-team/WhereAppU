import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity, DeviceEventEmitter, View, Text, Alert, NetInfo } from 'react-native';
import Contacts from 'react-native-contacts';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';

// BG Process
import BackgroundTask from 'react-native-background-task';

// Libs
import Permissions from '../../Lib/Permissions';
import SendMessage from '../../Lib/SendMessage';
// import NearbyConnection from '../../Lib/NearbyConnection';

// Components
import ContactsList from '../../Components/ContactsList/ContactsList';
import SendSOSButton from '../../Components/SendSOS/SendSOSButton';
import LinkButton from '../../Components/LinkButton/LinkButton';
import SelectEmergencyModal from '../../Components/Modals/SelectEmergencyModal';
import NotificationModal from '../../Components/Modals/NotificationModal';
import PhoneModal from '../../Components/Modals/PhoneModal';
import ConnectionModal from '../../Components/Modals/ConnectionModal';

// Styles
import styles from './DashboardViewStyles';
import { Colors } from '../../Themes';

// Reducers
import ContactsActions from '../../Redux/ContactsRedux';
import SOSActions from '../../Redux/SOSRedux';
import GeneralAlertsActions from '../../Redux/GeneralAlertsRedux';
import ProcessedAlertsActions from '../../Redux/ProcessedAlertsRedux';
import ModalsActions from '../../Redux/UI/ModalsRedux';
import UserDataActions from '../../Redux/UserDataRedux';
import StartupActions from '../../Redux/StartupRedux';
import SocketActions from '../../Redux/SocketRedux';
import ConnectionActions from '../../Redux/ConnectionRedux';
import PushNotificationActions from '../../Redux/PushNotificationRedux';

// Push Notifications
import PushNotificationAndroid from 'react-native-push-notification';
import { pushNotifications } from '../../Notifications';


BackgroundTask.define(() => {

  BackgroundTask.finish();
});

class DashboardView extends Component {
  componentDidMount() {
    // Checks on connection and connection type on start
    NetInfo.isConnected.fetch().then((isConnected) => {
      this.props.checkIfConnected(isConnected);
    });
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      this.props.updateConnectionType(connectionInfo.type);
    });

    // Adds an event listener when connection and connection type on changes
    NetInfo.addEventListener('connectionChange', connectionInfo => this.props.updateConnectionType(connectionInfo.type));
    NetInfo.isConnected.addEventListener('connectionChange', isConnected => this.props.checkIfConnected(isConnected));

    // Register all the valid actions for notifications here and add the action handler for each action
    PushNotificationAndroid.registerNotificationActions(['View on Map','Yes','No', 'I´m OK', 'I Need Support']);
    DeviceEventEmitter.addListener('notificationActionReceived', (action) => {
      console.tron.log('___ NEW NOT', action);
      this.props.newPushNotification(action);
    });

    // setTimeout(() => {
    //   pushNotifications.localNotification({
    //     'msg': `Hi, ${this.props.userData.user_id}, our system detects emergency alert(s) near to you`
    //   });
    // }, 7000);

    // setTimeout(() => {
    //   pushNotifications.localUserNotification({
    //     'msg': `Hi, your friend ${this.props.userData.user_id} is requesting help, is under risk of acadebe ir alguna maricada`
    //   });
    // }, 7000);
    // setTimeout(() => {
    //   pushNotifications.localLisenSyncNotification({
    //     'msg': `Hi, your friend ${this.props.userData.user_id} is asking for your status, are you OK?`
    //   });
    // }, 14000);

    BackgroundTask.schedule({
      period: 50
    });

    this.checkStatus();

    this.reviewAlertsEvery30();
    setInterval(this.reviewAlertsEvery30, 300000);
  }

  componentWillUnmount() {
    this.props.closeAllModals();
  }

  onPressHelp = () => {
    Permissions.hasPermission('SEND_SMS').then((hasSendSMSPermission) => {
      if (hasSendSMSPermission) {
        Object.values(this.props.selectedContacts.contactList).reduce((promiseChain, contact) => {

          const { givenName, middleName, familyName } = contact;
          let getContactDetails = {
            number: contact.phoneNumbers[0].number,
            you: `${givenName || ''} ${middleName || ''} ${familyName || ''}`,
            friend: this.props.userData.user_id
          };

          return promiseChain.then(chainResults =>
            SendMessage.send(getContactDetails).then(msg => [...chainResults, {
              msg,
              ...getContactDetails
            }]));
        }, Promise.resolve([]))
          .then((arrayOfResults) => {
            console.tron.log({
              name: `SMS sent to ${arrayOfResults.length} contacts`,
              value: arrayOfResults
            });
          });
      }
    });
  }

  ifUserIsInAlert() {
    console.tron.log('IF USER IS IN ALERT');
    this.handleHelp();
  }

  async reviewAlertsEvery30() {
    console.tron.log('REVIEW ALERTS NEAR TO YOU');
    this.props.getProcessedAlerts();
    setTimeout(() => {
      if (this.props.processedAlerts.matches.length > 0) {
        for (const alert of this.props.processedAlerts.matches) {
          this.alerts += alert.DISASTER_TYPE+',';
        }
        pushNotifications.localNotification({
          'msg': `Hi, ${this.props.userData.user_id}, our system detects ${this.props.processedAlerts.matches.length} emergency alert(s) near to you`
        });
      }
    }, 2000);
  }

  async checkStatus() {
    const status = await BackgroundTask.statusAsync();
    if (status.available) {
      this.props.getProcessedAlerts();
      setTimeout(() => {
        if (this.props.processedAlerts.matches.length > 0) {
          for (const alert of this.props.processedAlerts.matches) {
            this.alerts += alert.DISASTER_TYPE+',';
          }
          pushNotifications.localNotification({
            'msg': `Hi, ${this.props.userData.user_id}, our system detects ${this.props.processedAlerts.matches.length} emergency alert(s) near to you`
          });
        }
      }, 2000);
      return;
    }

    const reason = status.unavailableReason;
    if (reason === BackgroundTask.UNAVAILABLE_DENIED) {
      Alert.alert('Denied', 'Please enable background "Background App Refresh" for this app');
    } else if (reason === BackgroundTask.UNAVAILABLE_RESTRICTED) {
      Alert.alert('Restricted', 'Background tasks are restricted on your device');
    }
  }

  getPhoneContacts = () => {
    this.props.navigation.navigate('AddNewContact');

    Permissions.hasPermission('READ_CONTACTS').then((hasPermission) => {
      if (hasPermission) {
        Contacts.getAll((err, contacts) => {
          if (err) {
            throw err;
          }
          const contactsWithPhone = contacts.filter(value => value['phoneNumbers'].length > 0);
          this.props.updateContacts(contactsWithPhone.reduce((map, obj) => {
            map[obj.rawContactId] = { ...obj, isSelected: false };
            return map;
          }, {}));
        });
      }
    });
  };

  handleHelp = () => {
    this.props.selectEmergencyToggle();
  }

  checkOnContacts = () => {
    const { user_id, user_refs } = this.props.userData;

    console.tron.log('SEND SYNC user_refs:', user_refs);

    this.props.sendSync({ user_id, user_refs });
  }

  render() {

    const {
      selectEmergencyToggle,
      getUserEmergency,
      removeUserEmergency,
      sendSos,
      notificationToggle,
      updateUserId
    } = this.props;

    const selectedContacts = Object.values(this.props.selectedContacts.contactList || {});

    return (

      <LinearGradient
        colors={[Colors.gradPrimary, Colors.gradSecondary, Colors.gradPrimary]}
        locations={[0,0.5,1]}
        style={styles.mainContainer}
      >
        <SendSOSButton action={this.handleHelp} />

        {
          selectedContacts.length == 0 ?
            <View style={styles.welcome}><Text style={[styles.defaultText, styles.alertText]}>
              To begin please add your favorite contacts.
              {'\n\n'}
              You can also send a SOS message with your current position.
            </Text></View> : null
        }

        {
          selectedContacts.length > 0
            ? <ContactsList navigation={this.props.navigation} contacts={selectedContacts} />
            : <TouchableOpacity
              style={styles.button}
              onPress={this.getPhoneContacts}
            >
              <Icon name="address-book" size={25} color="#fff" style={styles.buttonIcon}/>
              <Text style={styles.buttonText}>Add contacts</Text>
            </TouchableOpacity>
        }
        <SelectEmergencyModal
          isVisible={this.props.modalVisibility.selectEmergencyModalVisible}
          closeAction={selectEmergencyToggle}
          mainAction={sendSos}
          emergencyTypeAction={getUserEmergency}
          removeEmergencyType={removeUserEmergency}
          userData={this.props.userData}
          onSOSSent={this.onPressHelp}
        />
        <NotificationModal
          isVisible={this.props.modalVisibility.notificationVisible}
          visibilityAction={notificationToggle}
          messageType={'Request'}
        />
        <PhoneModal
          isVisible={this.props.userData.user_id ? false : true}
          submitAction={updateUserId}
        />
        <ConnectionModal
          isVisible={this.props.modalVisibility.connectionModalVisible}
          closeAction={this.props.closeAllModals}
          type={this.props.modalVisibility.connectioModalType}
        />
        {
          selectedContacts.length > 0 ?
            <LinkButton action={this.checkOnContacts} text="Check on contacts"/> : null
        }
      </LinearGradient>

    );
  }
}

const mapStateToProps = state => ({
  selectedContacts: state.contactList.selectedContacts,
  sosAlert: state.sosAlert,
  generalAlerts: state.generalAlerts,
  processedAlerts: state.processedAlerts,
  modalVisibility: state.modals,
  userData: state.userData
});

const mapDispatchToProps = dispatch => ({
  updateContacts: contactList => dispatch(ContactsActions.updateContacts(contactList)),
  sendApiSOS: alertObj => dispatch(SOSActions.sosFetch(alertObj)),
  getDataAndSendSOS: () => dispatch(StartupActions.startup('user_action')),
  getGeneralAlerts: userId => dispatch(GeneralAlertsActions.generalAlertsFetch(userId)),
  sendSos: () => dispatch(SocketActions.sendSos()),
  sendSync: userRefs => dispatch(SocketActions.sendSync(userRefs)),
  getProcessedAlerts: () => dispatch(ProcessedAlertsActions.processedAlertsFetch()),
  getUserEmergency: emergencyType => dispatch(UserDataActions.getUserEmergency(emergencyType)),
  removeUserEmergency: () => dispatch(UserDataActions.removeUserEmergency()),
  updateUserId: inputValue => dispatch(UserDataActions.getDeviceUniqueId(inputValue)),
  selectEmergencyToggle: () => dispatch(ModalsActions.selectEmergencyToggle()),
  notificationToggle: bool => dispatch(ModalsActions.notificationToggle(bool)),
  closeAllModals: () => dispatch(ModalsActions.closeAllModals()),
  initTcpConnection: () => dispatch(ConnectionActions.initTcpConnection()),
  updateConnectionType: connectionType => dispatch(ConnectionActions.updateConnectionType(connectionType)),
  checkIfConnected: isConnected => dispatch(ConnectionActions.checkIfConnected(isConnected)),
  newPushNotification: notificationObj => dispatch(PushNotificationActions.newPushNotification(notificationObj))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardView);

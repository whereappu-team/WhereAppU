import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  alertText: {
    textAlign: 'center',
    paddingHorizontal: 5,
    paddingVertical: 10,
    fontSize: 18
  },
  welcome: {
    flex: 1
  }
});

import React, { Component } from 'react'
import { connect } from 'react-redux'
import LinearGradient from 'react-native-linear-gradient'
import SideMenu from 'react-native-side-menu';

import ReduxNavigation from '../Navigation/ReduxNavigation'
import StartupActions from '../Redux/StartupRedux'
import ReduxPersist from '../Config/ReduxPersist'

//Components
import Header from '../Components/Header/Header'
import Menu from '../Components/Menu/Menu'

// Styles
import styles from './Styles/RootContainerStyles'
import { Colors } from '../Themes'

class RootContainer extends Component {

  componentDidMount () {
    // if redux persist is not active fire startup action
    if (!ReduxPersist.active) {
      this.props.startup()
    }
  }

  constructor(props){
    super(props)
    this.state = {
      isOpen: false
    }
  }

  toggle(){
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  updateMenu(isOpen){
    this.setState({isOpen})
  }

  render () {
    return (
      <LinearGradient
          colors={[Colors.gradPrimary, Colors.gradSecondary, Colors.gradPrimary]}
          locations={[0,0.5,1]}
          style={styles.applicationView}
        >

        <SideMenu
          menu={<Menu toggle={this.toggle.bind(this)} />}
          isOpen={this.state.isOpen}
          onChange={(isOpen) => this.updateMenu(isOpen)}
          menuPosition={'right'}
        >
          <Header toggle={this.toggle.bind(this)} menuState={this.state.isOpen}/>
          <ReduxNavigation />
        </SideMenu>

      </LinearGradient>
    )
  }
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = dispatch => ({
  startup: () => dispatch(StartupActions.startup())
})

export default connect(null, mapDispatchToProps)(RootContainer)

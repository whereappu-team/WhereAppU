// React
import React from 'react';
import { connect } from 'react-redux';
import { NavigationActions, withNavigation } from 'react-navigation';

// Components
import ContactDetails from '../Components/ContactDetails/ContactDetails';

//Reducers
import ContactsActions from '../Redux/ContactsRedux';


const ContactView = ({ updateLoadedContacts, loadContact, navigation, navigateTo }) => {
  
  const givenName = navigation.getParam('givenName');
  const middleName = navigation.getParam('middleName');
  const familyName = navigation.getParam('familyName');
  const phone = navigation.getParam('phone');
  const status = navigation.getParam('status');
  const location = navigation.getParam('location');

  console.tron.log(updateLoadedContacts, loadContact, navigateTo, navigation, location);
  
  const { itemStatus, itemName, itemPhone, lastUpdated, itemLoc } = {
    ...navigation.getParam('contact', {
      itemStatus: status,
      itemName: `${givenName || ''} ${middleName || ''} ${familyName || ''}`,
      itemPhone: phone,
      lastUpdated: 1537758633, 
      itemLoc: {
        lat: 4.5797424, 
        lng: -74.1391837
      }
    })
  };

  return (
    <ContactDetails
      status={itemStatus}
      name={itemName}
      phone={itemPhone}
      lastUpdated={lastUpdated}
      loc={itemLoc}
    />
  );
};

// const mapDispatchToProps = (dispatch) => ({
//   updateLoadedContacts: (loadedContacts) =>
//     dispatch(ContactsActions.updateLoadedContacts(loadedContacts)),
//   navigateTo: (route) => dispatch(NavigationActions.navigate({ routeName: route }))
// });

// const mapStateToProps = (state) => ({
//   loadedContacts: state.loadedContacts.contacts.contactList
// });

export default withNavigation(ContactView);

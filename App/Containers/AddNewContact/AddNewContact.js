// React
import React from 'react';
import { connect } from 'react-redux';
import { FlatList, View, Text, TouchableOpacity } from 'react-native';
import { NavigationActions } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';

// Components
import ContactItem from '../../Components/ContactItem/ContactItem';

// Reducers
import ContactsActions from '../../Redux/ContactsRedux';

// Styles
import styles from './AddNewContactStyles';
import { Colors } from '../../Themes';

const AddNewContact = ({
  updateSelectedContacts,
  contactList,
  selectedContacts,
  navigateTo
}) => {

  const selectContact = (contact) => {
    const { rawContactId } = contact;
    let updatedSelectedContacts = { ...selectedContacts };
    if (updatedSelectedContacts.hasOwnProperty(rawContactId)) {
      delete updatedSelectedContacts[rawContactId];
    } else if (Object.values(selectedContacts).length < 5) {
      updatedSelectedContacts[rawContactId] = contact;
    }
    updateSelectedContacts(updatedSelectedContacts);
  };

  const setSelectedContacts = () => {
    navigateTo('DashboardView');
  };

  const contactListArray = Object.values(contactList);

  return (
    <LinearGradient
      colors={[Colors.gradPrimary, Colors.gradSecondary, Colors.gradPrimary]}
      locations={[0,0.5,1]}
      style={styles.contactsContainer}
    >

      {contactListArray.length === 0 ? (
        <View style={styles.flatListWrap}>
          <Text style={styles.flatTitle}>Loading contacts...</Text>
        </View>
      ): (
        <View style={styles.flatListWrap}>
          <Text style={styles.flatTitle}>Select up to 5 contacts</Text>
          <FlatList
            style={styles.flatList}
            data={contactListArray.sort((a, b) => {
              const aIndex = `${a.givenName || a.middleName || a.familyName || 'Z'}${a.rawContactId}`;
              const bIndex = `${b.givenName || b.middleName || b.familyName || 'Z'}${b.rawContactId}`;
              return (aIndex < bIndex) ? -1 : 1;
            })}
            extraData={selectedContacts}
            renderItem={({ item }) => (
              <ContactItem
                key={item.rawContactId}
                contact={item}
                selected={selectedContacts.hasOwnProperty(item.rawContactId)}
                action={selectContact}
              />
            )}
          />
        </View>
      )}

      <View style={styles.addContactButton}>
        <TouchableOpacity
          onPress={setSelectedContacts}
          style={styles.button}
        >
          <Icon name="users" size={20} color={Colors.white} style={styles.buttonIcon}/>
          <Text style={styles.buttonText}>Add contacts</Text>
        </TouchableOpacity>
      </View>

    </LinearGradient>
  );
};

const mapDispatchToProps = dispatch => ({
  updateSelectedContacts: contactList => dispatch(ContactsActions.updateSelectedContacts(contactList)),
  navigateTo: route => dispatch(NavigationActions.navigate({ routeName: route }))
});

const mapStateToProps = state => ({
  contactList: state.contactList.contacts.contactList,
  selectedContacts: state.contactList.selectedContacts.contactList
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddNewContact);

import { StyleSheet } from 'react-native'
import { Colors, ApplicationStyles} from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  contactsContainer: {
    flex: 1
  },
  flatTitle:{
    color: Colors.white,
    fontSize: 20,
    textAlign: 'center',
    padding: 20
  },
  flatList:{
    paddingHorizontal: 20
  },
  flatListWrap:{
    flex: 4,
    paddingVertical: 15
  },
  addContactButton:{
    alignItems: 'center', 
    justifyContent: 'center'
  }


});

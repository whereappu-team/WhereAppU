import PushNotification from 'react-native-push-notification';
import { PushNotificationIOS } from 'react-native';

const configure = () => {
  PushNotification.configure({

    onRegister: function(token) {
      //process token
    },

    onNotification: function(notification) {
      // process the notification
      // required on iOS only
      notification.finish(PushNotificationIOS.FetchResult.NoData);
    },

    permissions: {
      alert: true,
      badge: true,
      sound: true
    },

    popInitialNotification: true,
    requestPermissions: true

  });
};

const localNotification = (obj) => {
  PushNotification.localNotification({
    autoCancel: true,
    largeIcon: 'ic_launcher',
    smallIcon: 'ic_notification',
    bigText: obj.msg,
    subText: 'Stay alert',
    color: 'green',
    vibrate: true,
    vibration: 300,
    ongoing: false,
    title: 'WhereAppU Alert',
    message: obj.msg,
    playSound: true,
    soundName: 'default'
  });
};

const localUserNotification = (obj) => {
  PushNotification.localNotification({
    autoCancel: true,
    largeIcon: 'ic_launcher',
    smallIcon: 'ic_notification',
    bigText: obj.msg,
    subText: 'Friend Request',
    color: 'green',
    vibrate: true,
    vibration: 300,
    ongoing: false,
    title: 'WhereAppU Alert - Friend Request',
    message: obj.msg,
    playSound: true,
    soundName: 'default',
    actions: '["View on Map"]',
    mapData: obj.location
  });
};

const localLisenSyncNotification = (obj) => {
  PushNotification.localNotification({
    autoCancel: true,
    largeIcon: 'ic_launcher',
    smallIcon: 'ic_notification',
    bigText: obj.msg,
    subText: 'Friend Ask',
    color: 'green',
    vibrate: true,
    vibration: 1000,
    ongoing: false,
    title: 'WhereAppU Alert - Friend Ask',
    message: obj.msg,
    playSound: true,
    soundName: 'default',
    actions: '["I´m OK", "I Need Support"]'
  });
}

export {
  configure,
  localNotification,
  localUserNotification,
  localLisenSyncNotification
};
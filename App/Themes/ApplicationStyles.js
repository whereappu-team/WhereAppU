import Fonts from './Fonts'
import Metrics from './Metrics'
import Colors from './Colors'

// This file is for a reusable grouping of Theme items.
// Similar to an XML fragment layout in Android

const ApplicationStyles = {
  screen: {
    mainContainer: {
      flex: 1,
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    defaultText:{
      color: Colors.white,
      fontWeight: 'normal',
      fontSize: Fonts.size.regular,
    },
    button: {
      width: '100%',
      height: 60,
      flexWrap: 'nowrap',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.primaryOrange,
    },
    buttonText: {
      color: Colors.white,
      fontWeight: 'bold',
      fontSize: Fonts.size.h6,
    },
    buttonIcon: {
      marginRight: 5
    },
    container: {
      flex: 1,
      paddingTop: Metrics.baseMargin,
      backgroundColor: Colors.transparent
    },
    section: {
      margin: Metrics.section,
      padding: Metrics.baseMargin
    },
    sectionText: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.doubleBaseMargin,
      color: Colors.snow,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center'
    },
    subtitle: {
      color: Colors.snow,
      padding: Metrics.smallMargin,
      marginBottom: Metrics.smallMargin,
      marginHorizontal: Metrics.smallMargin
    },
    titleText: {
      ...Fonts.style.h2,
      fontSize: 14,
      color: Colors.text
    }
  },

  contact: {
    row: {
      borderWidth: 1,
      borderColor: Colors.white,
      borderRadius: 5,
      marginVertical: 3,
      padding: 3,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between'
    },
    left: {
      width: '90%',
      flexDirection: 'row',
      alignItems: 'center'
    },
    right: {
      width: '10%',
    },
    listContent: {
      width: '96%',
      paddingHorizontal: '3%',
      paddingVertical: 5,
    },
    name: {
      fontSize: 18,
      color: Colors.white
    },
    number: {
      fontSize: 14,
      color: Colors.white
    },
    info: {
      paddingHorizontal: 5,
    },
    userIcon:{
      marginHorizontal: 10
    }
  },

  darkLabelContainer: {
    padding: Metrics.smallMargin,
    paddingBottom: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    marginBottom: Metrics.baseMargin
  },
  darkLabel: {
    fontFamily: Fonts.type.bold,
    color: Colors.snow
  },
  groupContainer: {
    margin: Metrics.smallMargin,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  sectionTitle: {
    ...Fonts.style.h4,
    color: Colors.coal,
    backgroundColor: Colors.ricePaper,
    padding: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    marginHorizontal: Metrics.baseMargin,
    borderWidth: 1,
    borderColor: Colors.ember,
    alignItems: 'center',
    textAlign: 'center'
  },


}

export default ApplicationStyles

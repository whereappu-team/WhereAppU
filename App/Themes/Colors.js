const colors = {
  background: '#2E3540',
  primaryOrange: '#FE5438',
  secondaryColor: '#E5476D',
  black: "#2a2a2a",
  white: '#FFFFFF',
  gradPrimary: '#131619',
  gradSecondary: '#2E3540',
  danger: '#cf2a0e',



  green: '#8DC63F',
  clear: 'rgba(0,0,0,0)',
  facebook: '#3b5998',
  transparent: 'rgba(0,0,0,0)',
  silver: '#F7F7F7',
  steel: '#CCCCCC',
  error: 'rgba(200, 0, 0, 0.8)',
  ricePaper: 'rgba(255,255,255, 0.75)',
  frost: '#D8D8D8',
  cloud: 'rgba(200,200,200, 0.35)',
  windowTint: 'rgba(0, 0, 0, 0.4)',
  panther: '#161616',
  charcoal: '#595959',
  coal: '#2d2d2d',
  bloodOrange: '#fb5f26',
  snow: 'white',
  ember: 'rgba(164, 0, 48, 0.5)',
  fire: '#e73536',
  drawer: 'rgba(30, 30, 29, 0.95)',
  eggplant: '#251a34',
  border: '#483F53',
  banner: '#5F3E63',
  text: '#E0D7E5',
  orangePrimary: '#F98A2D',
  grayPrimary: '#4A4A4A',
  redPrimary: '#960000',
  graySecondary: '#ADADAD',
  orangeSecondary: '#F5A623'
 };

export default colors

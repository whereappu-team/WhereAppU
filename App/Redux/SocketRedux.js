import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  sendSos: null,
  sentSos: ['outbound'],
  failedSos: ['error'],
  broadcastedSos: null,
  receivedSos: ['inbound'],
  sendSync: null,
  sentSync: ['outbound'],
  failedSync: ['error'],
  broadcastedSync: null,
  receivedSync: ['inbound'],
  sendSyncStatus: ['payload'],
  sentSyncStatus: ['outbound'],
  failedSyncStatus: ['error'],
  broadcastedSyncStatus: null,
  receivedSyncStatus: ['inbound'],
  initListeners: null
});

export const SocketTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  SOS: {
    sending: false,
    broadcasting: false,
    error: null,
    inbound: null,
    outbound: null
  },
  sync: {
    sending: false,
    broadcasting: false,
    error: null,
    inbound: null,
    outbound: null
  },
  syncStatus: {
    sending: false,
    broadcasting: false,
    error: null,
    inbound: null,
    outbound: null
  },
  listenersInitialized: false
});

/* ------------- Reducers ------------- */

// SOS

export const sendSOS = state =>
  state.merge({ SOS: { ...state.SOS, sending: true, broadcasting: false } });

export const sentSOS = (state, { outbound }) =>
  state.merge({ SOS: { ...state.SOS, sending: false, broadcasting: true, outbound, error: null } });

export const failedSOS = (state, { error }) =>
  state.merge({ SOS: { ...state.SOS, sending: false, broadcasting: false, error } });

export const broadcastedSOS = state =>
  state.merge({ SOS: { ...state.SOS, sending: false, broadcasting: false, error: null } });

export const receivedSOS = (state, { inbound }) =>
  state.merge({ SOS: { ...state.SOS, inbound } });

// Sync

export const sendSync = state =>
  state.merge({ sync: { ...state.sync, sending: true, broadcasting: false } });

export const sentSync = (state, { outbound }) =>
  state.merge({ sync: { ...state.sync, sending: false, broadcasting: true, outbound, error: null } });

export const failedSync = (state, { error }) =>
  state.merge({ sync: { ...state.sync, sending: false, broadcasting: false, error } });

export const broadcastedSync = state =>
  state.merge({ sync: { ...state.sync, sending: false, broadcasting: false, error: null } });

export const receivedSync = (state, { inbound }) =>
  state.merge({ sync: { ...state.sync, inbound } });

// Sync Status

export const sendSyncStatus = state =>
  state.merge({ syncStatus: { ...state.syncStatus, sending: true, broadcasting: false } });

export const sentSyncStatus = (state, { outbound }) =>
  state.merge({ syncStatus: { ...state.syncStatus, sending: false, broadcasting: true, outbound, error: null } });

export const failedSyncStatus = (state, { error }) =>
  state.merge({ syncStatus: { ...state.syncStatus, sending: false, broadcasting: false, error } });

export const broadcastedSyncStatus = state =>
  state.merge({ syncStatus: { ...state.syncStatus, sending: false, broadcasting: false, error: null } });

export const receivedSyncStatus = (state, { inbound }) =>
  state.merge({ syncStatus: { ...state.syncStatus, inbound } });

export const initListeners = state =>
  state.merge({ listenersInitialized: true });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SEND_SOS]: sendSOS,
  [Types.SENT_SOS]: sentSOS,
  [Types.FAILED_SOS]: failedSOS,
  [Types.BROADCASTED_SOS]: broadcastedSOS,
  [Types.RECEIVED_SOS]: receivedSOS,
  [Types.SEND_SYNC]: sendSync,
  [Types.SENT_SYNC]: sentSync,
  [Types.FAILED_SYNC]: failedSync,
  [Types.BROADCASTED_SYNC]: broadcastedSync,
  [Types.RECEIVED_SYNC]: receivedSync,
  [Types.SEND_SYNC_STATUS]: sendSyncStatus,
  [Types.SENT_SYNC_STATUS]: sentSyncStatus,
  [Types.FAILED_SYNC_STATUS]: failedSyncStatus,
  [Types.BROADCASTED_SYNC_STATUS]: broadcastedSyncStatus,
  [Types.RECEIVED_SYNC_STATUS]: receivedSyncStatus,
  [Types.INIT_LISTENERS]: initListeners
});

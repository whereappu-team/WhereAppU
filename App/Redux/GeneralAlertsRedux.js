import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  generalAlertsFetch: ['userId'],
  generalAlertsSuccess: ['response'],
  generalAlertsFailure: ['error']
})

export const GeneralAlertsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  response: null,
  fetching: false,
  error: null
})

/* ------------- Selectors ------------- */

export const generalAlertsSelectors = {
  selectGeneralAlerts: state => state.generalAlerts
}

/* ------------- Reducers ------------- */

export const fetch = (state, { userId }) =>
  state.merge({ fetching: true, userId })

export const success = (state, response) => {
  return state.merge({ fetching: false, error: null, response })
}

export const failure = (state, { error }) =>
  state.merge({ fetching: false, error })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GENERAL_ALERTS_FETCH]: fetch,
  [Types.GENERAL_ALERTS_SUCCESS]: success,
  [Types.GENERAL_ALERTS_FAILURE]: failure
})

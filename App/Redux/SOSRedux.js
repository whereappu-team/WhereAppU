import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  sosFetch: ['payload'],
  sosSuccess: ['response'],
  sosFailure: ['error']
})

export const SOSTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  response: null,
  fetching: false,
  error: null
})

/* ------------- Selectors ------------- */
export const sosSelectors = {
  selectSOS: state => state.sos
}

/* ------------- Reducers ------------- */
export const fetch = (state, { payload }) =>
  state.merge({ fetching: true, payload })

export const success = (state, response) => {
  return state.merge({ fetching: false, error: null, response })
}

export const failure = (state, { error }) =>
  state.merge({ fetching: false, error })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.SOS_FETCH]: fetch,
  [Types.SOS_SUCCESS]: success,
  [Types.SOS_FAILURE]: failure
})

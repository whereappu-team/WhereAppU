import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  selectEmergencyToggle: null,
  notificationToggle: ['bool'],
  closeAllModals: null,
  openConnectionModal: ['connectioModalType']
})

export const ModalsTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
	selectEmergencyModalVisible: false,
  notificationVisible: false,
  connectionModalVisible: false,
  connectioModalType: null
})

/* ------------- Selectors ------------- */
export const modalsSelectors = {
  selectModals: state => state.modals
}

/* ------------- Reducers ------------- */
export const selectEmergencyToggle = state =>
	state.merge({
		selectEmergencyModalVisible: !state.selectEmergencyModalVisible
	})

export const notificationToggle = (state, { bool }) =>
  state.merge({
    notificationVisible: bool
  })

export const openConnectionModal = (state, { connectioModalType }) =>
  state.merge({
    connectionModalVisible: true,
    connectioModalType
  })

export const closeAllModals = state =>
  state.merge({
    selectEmergencyModalVisible: false,
    notificationVisible: false,
    connectionModalVisible: false
  })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.SELECT_EMERGENCY_TOGGLE]: selectEmergencyToggle,
  [Types.NOTIFICATION_TOGGLE]: notificationToggle,
  [Types.CLOSE_ALL_MODALS]: closeAllModals,
  [Types.OPEN_CONNECTION_MODAL]: openConnectionModal
})

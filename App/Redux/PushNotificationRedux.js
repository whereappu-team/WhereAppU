import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
	newPushNotification: ['pushNotification']
})

export const PushNotificationTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  pushNotification: null
})

/* ------------- Selectors ------------- */

export const pushNotificationSelectors = {
  selectPushNotification: state => state.pushNotification
}

/* ------------- Reducers ------------- */

export const newPushNotification = (state, { pushNotification }) =>
  state.merge({ pushNotification })


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.NEW_PUSH_NOTIFICATION]: newPushNotification
})

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  updateConnectionType: ['connectionType'],
	initTcpConnection: null,
	checkIfConnected: ['isConnected']
})

export const ConnectionTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
	connectionType: null,
	isConnected: false,
	tcpConnected: false
})

/* ------------- Selectors ------------- */
export const connectionSelectors = {
  selectConnection: state => state.connection
}

/* ------------- Reducers ------------- */
export const initTcpConnection = state =>
	state.merge({ tcpConnected: true })

export const updateConnectionType = (state, { connectionType }) =>
	state.merge({ connectionType })

export const checkIfConnected = (state, { isConnected }) =>
	state.merge({ isConnected })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
	[Types.UPDATE_CONNECTION_TYPE]: updateConnectionType,
	[Types.INIT_TCP_CONNECTION]: initTcpConnection,
	[Types.CHECK_IF_CONNECTED]: checkIfConnected
})

import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  getDeviceUniqueId: ['user_id'],
  parseUniqueId: ['user_id'],
  getSelectedContacts: ['user_refs'],
  getUserLocation: ['user_loc'],
  getUserEmergency: ['disaster_type'],
  removeUserEmergency: null
});

export const UserDataTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  user_id: '',
  user_refs: [],
  user_loc: {},
  disaster_type: 'unknown'
});

/* ------------- Selectors ------------- */
export const userDataSelectors = {
  selectUserData: state => state.userData
};

/* ------------- Reducers ------------- */
export const updateUniqueID = (state, { user_id }) =>
  state.merge({ user_id });

export const updateSelectedContacts = (state, { user_refs }) =>
  state.merge({ user_refs });

export const updateUserLocation = (state, { user_loc }) =>
  state.merge({ user_loc });

export const updateUserEmergency = (state, { disaster_type }) =>
  state.merge({ disaster_type });

export const removeUserEmergency = state =>
  state.merge({ disaster_type: 'unknown' });

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_DEVICE_UNIQUE_ID]: updateUniqueID,
  [Types.PARSE_UNIQUE_ID]: updateUniqueID,
  [Types.GET_SELECTED_CONTACTS]: updateSelectedContacts,
  [Types.GET_USER_LOCATION]: updateUserLocation,
  [Types.GET_USER_EMERGENCY]: updateUserEmergency,
  [Types.REMOVE_USER_EMERGENCY]: removeUserEmergency
});

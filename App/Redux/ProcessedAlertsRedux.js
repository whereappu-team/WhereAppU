import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  processedAlertsFetch: ['payload'],
  processedAlertsSuccess: ['response'],
  processedAlertsFailure: ['error'],
  updateMatchedAlerts: ['matches']
})

export const ProcessedAlertsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  response: null,
  fetching: false,
  error: null,
  matches: []
})

/* ------------- Selectors ------------- */

export const processedAlertsSelectors = {
  selectProcessedAlerts: state => state.processedAlerts
}

/* ------------- Reducers ------------- */

export const fetch = state =>
  state.merge({ fetching: true })

export const success = (state, response) => {
  return state.merge({ fetching: false, error: null, response })
}

export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

export const updateMatchedAlerts = (state, { matches }) =>
  state.merge({ fetching: false, matches: [...matches] });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PROCESSED_ALERTS_FETCH]: fetch,
  [Types.PROCESSED_ALERTS_SUCCESS]: success,
  [Types.PROCESSED_ALERTS_FAILURE]: failure,
  [Types.UPDATE_MATCHED_ALERTS]: updateMatchedAlerts
})

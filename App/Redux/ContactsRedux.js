import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  updateContacts: ['contactList'],
  updateSelectedContacts: ['contactList'],
  toggleContactState: ['rawContactId']
});

export const ContactsTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  selectedContacts: {
    contactList: {}
  },
  contacts: {
    contactList: {}
  }
});

/* ------------- Selectors ------------- */

export const contactsSelectors = {
  allContacts: state => state.contactList.contacts.contactList,
  selectContacts: state => state.contactList.selectedContacts.contactList
};

/* ------------- Reducers ------------- */

export const updateContacts = (state, contactList) => ({
  ...state,
  contacts: {
    ...contactList
  }
});

export const updateSelectedContacts = (state, contactList) => ({
  ...state,
  selectedContacts: {
    ...contactList
  }
});

export const toggleContactState = (state, { rawContactId }) => {
  let newContactsContactListState = { ...state.contacts.contactList, [rawContactId]: { ...state.contacts.contactList[rawContactId], isSelected: !state.contacts.contactList[rawContactId].isSelected } };
  return { ...state, contacts: { ...state.contacts, contactList: newContactsContactListState } };
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.UPDATE_CONTACTS]: updateContacts,
  [Types.UPDATE_SELECTED_CONTACTS]: updateSelectedContacts,
  [Types.TOGGLE_CONTACT_STATE]: toggleContactState
});

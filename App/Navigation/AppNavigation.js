import { StackNavigator } from 'react-navigation';
import LaunchScreen from '../Containers/LaunchScreen/LaunchScreen';
import DashboardView from '../Containers/DashboardView/DashboardView';
import ContactView from '../Containers/ContactView';
import AddNewContact from '../Containers/AddNewContact/AddNewContact';
import CustomMapView from '../Components/CustomMapView';

import styles from './Styles/NavigationStyles';

// Manifest of possible screens
const PrimaryNav = StackNavigator(
  {
    DashboardView: {
      screen: DashboardView,
      navigationOptions: () => ({
        title: 'Hola mundo'
      })
    },
    LaunchScreen: { screen: LaunchScreen },
    ContactView: { screen: ContactView },
    AddNewContact: {screen: AddNewContact}
  },
  {
    // Default config for all screens
    headerMode: 'none',
    initialRouteName: 'LaunchScreen',
    navigationOptions: {
      headerStyle: styles.header
    }
  }
);

export default PrimaryNav;

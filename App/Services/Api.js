import apisauce from 'apisauce'

const create = (baseURL = 'https://whereappu-alerts.mybluemix.net/api/') => {

  const api = apisauce.create({
    baseURL,
    headers: {
      'Content-Type': 'application/json',
      'X-IBM-Client-Id': 'f06fd804-9ac0-4970-83ee-f6429cd14614'
    },
    timeout: 10000
  })

  const sendSOSAlert = userPayload => api.post('send-alert', userPayload)
  const getGeneralAlerts = userId => api.get(`get-alerts?user_id=${userId}&type=app`)
  const getProcessedAlerts = () => api.get('get-alert-stack')

  return {
    sendSOSAlert,
    getGeneralAlerts,
    getProcessedAlerts
  }
}

// let's return back our create method as the default.
export default {
  create
}

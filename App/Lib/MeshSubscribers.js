import { DeviceEventEmitter } from 'react-native';

export default class MeshSubscribers {
  static subscribe(callback) {
    if (typeof callback === 'function') {
      DeviceEventEmitter.addListener('onMessageReceived', (message) => {
        console.log('onMessageReceived:', message);
        callback(message);
      });

      // This event is launched when a broadcast message has been received, the structure
      // of the dictionary received is explained in the appendix.
      DeviceEventEmitter.addListener('onBroadcastMessageReceived', (message) => {
        console.log('onMessageReceived:', message);
        callback(message);
      });
    }
  }

  static all() {
    //
    // Message listeners
    //

    // This event is launched when a message has been received,
    // the `message` dictionary structure is explained in the appendix
    DeviceEventEmitter.addListener('onMessageReceived', (message) => {
      console.log('onMessageReceived:', message);
    });

    // This event is launched when a broadcast message has been received, the structure
    // of the dictionary received is explained in the appendix.
    DeviceEventEmitter.addListener('onBroadcastMessageReceived', (message) => {
      console.log('onMessageReceived:', message);
    });

    // This event is launched when a message could not be sent, it receives an error
    // whose structure will be explained in the appendix
    DeviceEventEmitter.addListener('onMessageFailed', (error) => {
      console.log('onMessageFailed:', error);

      // console.log('code', error.conde); // error code
      // console.log('message', error.message); // message object
      // console.log('description', error.description); // Error cause
    });

    // This event is launched when a message was sent, contains the message
    // itself, and the structure of message is explained in the appendix.
    DeviceEventEmitter.addListener('onMessageSent', (message) => {
      console.log('onMessageSent:', message);
    });

    // This event is launched when a message was received but it contains errors,
    // the structure for this kind of error is explained in the appendix.
    // This method is launched exclusively on Android.
    DeviceEventEmitter.addListener('onMessageReceivedException', (error) => {
      console.log('onMessageReceivedException:', error);
      // console.log('sender: ', error.sender); // User ID of the sender
      // console.log('code: ', error.code); // error code
      // console.log('message', error.message); // message object empty
      // console.log('description', error.description); // Error cause
    });

    //
    // Device listeners
    //

    // This event is launched when the service has been started successfully, it receives
    // a device dictionary that will be descripted in the appendix.
    DeviceEventEmitter.addListener('onStarted', (device) => {
      console.log('onStarted:', device);
    });

    // This event is launched when the Bridgefy service fails on the start, it receives
    // a dictionary (error) that will be explained in the appendix.
    DeviceEventEmitter.addListener('onStartError', (error) => {
      console.log('onStartError:', error);
      // console.log('code: ', error.conde); // error code
      // console.log('description', error.description); // Error cause
    });

    // This event is launched when the Bridgefy service stops.
    DeviceEventEmitter.addListener('onStopped', () => {
      console.log('onStopped');
    });

    // This method is launched when a device is nearby and has established connection with the local user.
    // It receives a device dictionary.
    DeviceEventEmitter.addListener('onDeviceConnected', (device) => {
      // BridgefyClient.deviceList.push(device);
      console.log('onDeviceConnected:', device);
    });
    // This method is launched when there is a disconnection of a user.
    DeviceEventEmitter.addListener('onDeviceLost', (device) => {
      console.log('onDeviceLost:', device);
    });

    // This is method is launched exclusively on iOS devices, notifies about certain actions like when
    // the bluetooth interface  needs to be activated, when internet is needed and others.
    DeviceEventEmitter.addListener('onEventOccurred', (event) => {
      console.log('onEventOccurred:', event);
    });
  }
}

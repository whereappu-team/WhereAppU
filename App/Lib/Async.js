export const to = (promise) => {
  return promise.then((data) => [null, data], (err) => [err]);
};

export const reflect = (promise) => {
  return promise.then((data) => data, (err) => err);
};

export default {
  to,
  reflect
};

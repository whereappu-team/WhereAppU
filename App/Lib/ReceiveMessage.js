import { Alert } from 'react-native';

import SmsListener from 'react-native-android-sms-listener';

export default class ReceiveMessage {
  subscription = {};

  static subscribe(callback) {
    console.log('The device is listening for SMS');
    this.subscription = SmsListener.addListener((message) => {
      const { id, latitude, longitude, isSOS } = JSON.parse(message.body);
      // const location = message.body.match(/: \d+/g).map((s) => s.replace(/: /g, ''));
      // const latitude = location[0];
      // const longitude = location[1];
      // if (message.body.includes('Hey, this is an S.O.S message')) {
      //   Alert.alert(
      //     `S.O.S. Message received from ${
      //       message.originatingAddress
      //     }. The location is: latitude: ${latitude}, longitude: ${longitude}`
      //   );
      // }
      if (typeof callback === 'function') {
        callback({
          id,
          latitude,
          longitude,
          isSOS,
          isConfirmed: true
        });
      }
      console.log('Message received');
      console.info(message);
    });
  }

  static unsuscribe() {
    console.log('The device is not listening for SMS anymore');
    this.subscription.remove();
  }
}

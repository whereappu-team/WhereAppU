import net from 'react-native-tcp';

import { to } from '../Lib/Async';

const WAU_WIFI_IP = '192.168.4.1';
const WAU_WIFI_PORT = 80;

class TcpClient {
    client = null;
    isReady = false;

    sendSOS = async (msgObj) => {
      if (!this.isReady) {
        let [, err] = await to(this.init());
        if (err) {
          throw new Error('Tcp client could not establish connection');
        }
      }

      const { user_id, user_refs, user_loc, disaster_type } = msgObj;
      this.client.write(`[SOS]${user_id}|${user_refs.join(',')}|${user_loc.lat}|${user_loc.lng}|${disaster_type};`);
      new Promise((resolve, reject) => {
        const sosListener = (response) => {
          this.client.off('error', sosErrorListener);
          return resolve(response);
        };
        const sosErrorListener = (error) => {
          this.client.off('data', sosListener);
          return reject(new Error(error));
        };
        this.client.once('data', sosListener);
        this.client.once('error', sosErrorListener);
      });
    }

    listen = () => {
      this.client.on('connect', () => {
        console.tron.log('[TCP] connected');
      }).on('ready', () => {
        this.isReady = true;
        console.tron.log('[TCP] ready');
      }).on('data', (data) => {
        console.tron.log('[TCP] received', data);
      }).on('end', () => {
        this.isReady = false;
        console.tron.log('[TCP] disconnected');
      }).on('error', (error) => {
        console.tron.log('[TCP] error', error);
      }).on('timeout', () => {
        this.isReady = false;
        this.init();
        console.tron.log('[TCP] reinit');
      });
    }

    init = () => {
      this.client = net.createConnection(WAU_WIFI_PORT, WAU_WIFI_IP);
      return new Promise((resolve, reject) => {
        const initListener = () => {
          this.client.off('error', initErrorListener);
          return resolve(true);
        };
        const initErrorListener = (error) => {
          this.client.off('connect', initListener);
          return reject(new Error(error));
        };
        this.client
          .off('connect').off('ready').off('data').off('end').off('error').off('timeout')
          .once('connect', initListener)
          .once('error', initErrorListener);
        this.listen();
      });
    }
}

export default TcpClient;
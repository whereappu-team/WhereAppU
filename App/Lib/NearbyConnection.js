import NearbyConnection from 'react-native-google-nearby-connection';

console.tron.log('[Nearby Connection] IMPORTS');

const SERVICE_ID = 'WAU';


//Callbacks

//Endpoint Discovery
// NearbyConnection.onDiscoveryStarting(({
//   SERVICE_ID               // A unique identifier for the service
// }) => {
//   // Discovery services is starting
//   console.tron.log('[Nearby Connection]');
// });

// NearbyConnection.onDiscoveryStarted(({
//   SERVICE_ID               // A unique identifier for the service
// }) => {
//   // Discovery services has started
//   console.tron.log('[Nearby Connection]');
// });

// NearbyConnection.onDiscoveryStartFailed(({
//   SERVICE_ID,               // A unique identifier for the service
//   statusCode              // The status of the response [See CommonStatusCodes](https://developers.google.com/android/reference/com/google/android/gms/common/api/CommonStatusCodes)
// }) => {
//   // Failed to start discovery service
//   console.tron.log('[Nearby Connection]');
// });

// // Note - Can take up to 3 min to time out
// NearbyConnection.onEndpointLost(({
//   endpointId,             // ID of the endpoint we lost
//   endpointName,           // The name of the remote device we lost
//   SERVICE_ID               // A unique identifier for the service
// }) => {
//   // Endpoint moved out of range or disconnected
//   console.tron.log('[Nearby Connection]');
// });

// NearbyConnection.onEndpointDiscovered(({
//   endpointId,             // ID of the endpoint wishing to connect
//   endpointName,           // The name of the remote device we're connecting to.
//   SERVICE_ID               // A unique identifier for the service
// }) => {
//   // An endpoint has been discovered we can connect to
//   console.tron.log('[Nearby Connection]');
// });

// // Endpoint Advertisement
// NearbyConnection.onAdvertisingStarting(({
//   endpointName,            // The name of the service thats starting to advertise
//   SERVICE_ID               // A unique identifier for the service
// }) => {
//   // Advertising service is starting
//   console.tron.log('[Nearby Connection]');
// });

// NearbyConnection.onAdvertisingStarted(({
//   endpointName,            // The name of the service thats started to advertise
//   SERVICE_ID               // A unique identifier for the service
// }) => {
//   // Advertising service has started
//   console.tron.log('[Nearby Connection]');
// });

// NearbyConnection.onAdvertisingStartFailed(({
//   endpointName,            // The name of the service thats failed to start to advertising
//   SERVICE_ID,               // A unique identifier for the service
//   statusCode              // The status of the response [See CommonStatusCodes](https://developers.google.com/android/reference/com/google/android/gms/common/api/CommonStatusCodes)
// }) => {
//   // Failed to start advertising service
//   console.tron.log('[Nearby Connection]');
// });

// // Connection negotiation
// NearbyConnection.onConnectionInitiatedToEndpoint(({
//   endpointId,             // ID of the endpoint wishing to connect
//   endpointName,           // The name of the remote device we're connecting to.
//   authenticationToken,    // A small symmetrical token that has been given to both devices.
//   SERVICE_ID,              // A unique identifier for the service
//   incomingConnection      // True if the connection request was initated from a remote device.
// }) => {
//   // Connection has been initated
//   console.tron.log('[Nearby Connection]');
// });

// NearbyConnection.onConnectedToEndpoint(({
//   endpointId,             // ID of the endpoint we connected to
//   endpointName,           // The name of the service
//   SERVICE_ID              // A unique identifier for the service
// }) => {
//   // Succesful connection to an endpoint established
//   console.tron.log('[Nearby Connection]');
// });

// NearbyConnection.onEndpointConnectionFailed(({
//   endpointId,             // ID of the endpoint we failed to connect to
//   endpointName,           // The name of the service
//   SERVICE_ID,              // A unique identifier for the service
//   statusCode              // The status of the response [See CommonStatusCodes](https://developers.google.com/android/reference/com/google/android/gms/common/api/CommonStatusCodes)
// }) => {
//   // Failed to connect to an endpoint
//   console.tron.log('[Nearby Connection]');
// });

// NearbyConnection.onDisconnectedFromEndpoint(({
//   endpointId,             // ID of the endpoint we disconnected from
//   endpointName,           // The name of the service
//   SERVICE_ID              // A unique identifier for the service
// }) => {
//   // Disconnected from an endpoint
//   console.tron.log('[Nearby Connection]');
// });

// //Payload Status
// NearbyConnection.onReceivePayload(({
//   SERVICE_ID,              // A unique identifier for the service
//   endpointId,             // ID of the endpoint we got the payload from
//   payloadType,            // The type of this payload (File or a Stream) [See Payload](https://developers.google.com/android/reference/com/google/android/gms/nearby/connection/Payload)
//   payloadId               // Unique identifier of the payload
// }) => {
//   // Payload has been received
//   console.tron.log('[Nearby Connection]');
// });

// NearbyConnection.onPayloadUpdate(({
//   SERVICE_ID,              // A unique identifier for the service
//   endpointId,             // ID of the endpoint we got the payload from
//   bytesTransferred,       // Bytes transfered so far
//   totalBytes,             // Total bytes to transfer
//   payloadId,              // Unique identifier of the payload
//   payloadStatus,          // [See PayloadTransferUpdate.Status](https://developers.google.com/android/reference/com/google/android/gms/nearby/connection/PayloadTransferUpdate.Status)
//   payloadHashCode        // ???
// }) => {
//   // Update on a previously received payload
//   console.tron.log('[Nearby Connection]');
// });

// NearbyConnection.onSendPayloadFailed(({
//   SERVICE_ID,              // A unique identifier for the service
//   endpointId,             // ID of the endpoint wishing to connect
//   statusCode              // The status of the response [See CommonStatusCodes](https://developers.google.com/android/reference/com/google/android/gms/common/api/CommonStatusCodes)
// }) => {
//   // Failed to send a payload
//   console.tron.log('[Nearby Connection]');
// });






// Starting the discovery service
// NearbyConnection.startDiscovering(SERVICE_ID);

// Stopping the discovery service
// NearbyConnection.stopDiscovering(SERVICE_ID);

// Whether a service is currently discovering
// NearbyConnection.isDiscovering();

// Connect to a discovered endpoint
// NearbyConnection.connectToEndpoint(
//   SERVICE_ID,              // A unique identifier for the service
//   endpointId              // ID of the endpoint to connect to
// );

//Disconnect from an endpoint
// NearbyConnection.disconnectFromEndpoint(
//   SERVICE_ID,              // A unique identifier for the service
//   endpointId              // ID of the endpoint we wish to disconnect from
// );

//Starting the advertising service
// NearbyConnection.startAdvertising(
//   'test',           // This nodes endpoint name
//   SERVICE_ID,              // A unique identifier for the service
//   'P2P_CLUSTER'                // The Strategy to be used when discovering or advertising to Nearby devices [See Strategy](https://developers.google.com/android/reference/com/google/android/gms/nearby/connection/Strategy)
// );

// Stopping the advertising service
// NearbyConnection.stopAdvertising(SERVICE_ID);

//Whether a service is currently advertising
// NearbyConnection.isAdvertising();

//Accepting a connection from an endpoint
// NearbyConnection.acceptConnection(
//   SERVICE_ID,               // A unique identifier for the service
//   endpointId               // ID of the endpoint wishing to accept the connection from
// );

//Rejecting a connection from an endpoint
// NearbyConnection.rejectConnection(
//   SERVICE_ID,               // A unique identifier for the service
//   endpointId               // ID of the endpoint wishing to reject the connection from
// );

//Removes a payload (free memory)
// NearbyConnection.removePayload(
//   SERVICE_ID,               // A unique identifier for the service
//   endpointId,              // ID of the endpoint wishing to stop playing audio from
//   payloadId                // Unique identifier of the payload
// );

//Send a bytes payload (Payload.BYTES)
// NearbyConnection.sendBytes(
//   SERVICE_ID,               // A unique identifier for the service
//   endpointId,              // ID of the endpoint wishing to stop playing audio from
//   bytes                    // A string of bytes to send
// );

//Read payload [Payload.Type.BYTES] or out of band file [Payload.Type.FILE] or stream [Payload.Type.STREAM] information
// NearbyConnection.readBytes(
//   SERVICE_ID,               // A unique identifier for the service
//   endpointId,              // ID of the endpoint wishing to stop playing audio from
//   payloadId                // Unique identifier of the payload
// ).then(({
//   type,                    // The Payload.Type represented by this payload
//   bytes,                   // [Payload.Type.BYTES] The bytes string that was sent
//   payloadId,               // [Payload.Type.FILE or Payload.Type.STREAM] The payloadId of the payload this payload is describing
//   filename,                // [Payload.Type.FILE] The name of the file being sent
//   metadata,                // [Payload.Type.FILE] The metadata sent along with the file
//   streamType              // [Payload.Type.STREAM] The type of stream this is [audio or video]
// }) => {
//   console.tron.log(
//     '[Nearby Connection] read bytes',
//     type,
//     bytes,
//     payloadId,
//     filename,
//     metadata,
//     streamType
//   );
// });


export default NearbyConnection;
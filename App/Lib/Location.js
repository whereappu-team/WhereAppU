import Geolocation from 'react-native-geolocation-service';
import Permissions from '../Lib/Permissions';

const returnError = (error) => {
  console.tron.log({
    name: 'DERP, GPS error do somethign with the state when no GPS is enabled or something bra',
    value: error
  });
  return error;
};

export default class Location {

  static getCurrentLocation() {
    const promise = new Promise((resolve, reject) => {
      Permissions.hasPermission('ACCESS_FINE_LOCATION').then((hasPermission) => {
        if (hasPermission) {
          Geolocation.getCurrentPosition(
            (position) => {
              resolve(position);
            },
            (error) => {
              returnError(error);
            }, {
              enableHighAccuracy: true,
              timeout: 30000,
              maximumAge: 10000
            }
          );
        }
      }).catch(() => {
        console.warn('warn');
      });
    });
    return promise;
  }
}

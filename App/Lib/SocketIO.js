import io from 'socket.io-client';

import { to } from './Async';

const SOCKETIO_TOKEN = 'a6075ba8-6096-48fa-ae7b-43cf28a62c8c';

class Socket {
  constructor({ host, path }) {
    this.socket = io(host, { path });
  }

  isAuth = null;

  auth = (userId) => {
    if (this.isAuth) {
      return true;
    }
    this.socket.emit('authentication', { token: SOCKETIO_TOKEN, user_id: userId });
    console.tron.log('SOCKETIO -->', 'authentication');
    return new Promise((resolve, reject) => {
      const authListener = () => {
        console.tron.log('SOCKETIO <--', 'authentication/ok');
        this.socket.off('authentication/unauthorized', authUnauthorizedListener);
        this.isAuth = true;
        return resolve(this.isAuth);
      };
      const authUnauthorizedListener = () => {
        console.tron.log('SOCKETIO <--', 'authentication/unauthorized');
        this.socket.off('authentication/ok', authListener);
        this.isAuth = false;
        return reject(this.isAuth);
      };
      this.socket.once('authentication/unauthorized', authUnauthorizedListener);
      this.socket.once('authentication/ok', authListener);
    });
  }

  sendSOS = async (sosData) => {
    if (!this.isAuth) {
      let [err] = await to(this.auth());
      if (err) {
        throw err;
      }
    }

    this.socket.emit('sos', sosData);
    console.tron.log('SOCKETIO -->', 'sos', sosData);

    return new Promise((resolve) => {
      this.socket
        .once('sos/invalid-data', () => {
          console.tron.log('SOCKETIO <--', 'sos/invalid-data');
          resolve(new Error('invalid data'));
        })
        .once('sos/received', () => {
          console.tron.log('SOCKETIO <--', 'sos/received');
          resolve(true);
        });
    });
  }

  trackSOS = () => {
    return new Promise((resolve) => {
      this.socket
        .once('sos/transmitted', () => {
          console.tron.log('SOCKETIO <--', 'sos/transmitted');
          resolve(true);
        });
    });
  }

  listenSOS = () => {
    return new Promise((resolve) => {

      console.tron.log('SOCKETIO', 'listening sos');

      this.socket
        .on('sos-broadcast', (userRefs) => {
          console.tron.log('SOCKETIO <--', 'sos-broadcast', userRefs);
          resolve(userRefs);
        });
    });
  }

  sendSync = async (syncData) => {
    if (!this.isAuth) {
      let [err] = await to(this.auth());
      if (err) {
        throw err;
      }
    }

    this.socket.emit('sync', syncData);
    console.tron.log('SOCKETIO -->', 'sync', syncData);

    return new Promise((resolve) => {
      this.socket
        .once('sync/invalid-data', () => {
          console.tron.log('SOCKETIO <--', 'sync/invalid-data');
          resolve(new Error('invalid data'));
        })
        .once('sync/received', () => {
          console.tron.log('SOCKETIO <--', 'sync/received');
          resolve(true);
        });
    });
  }

  trackSync = () => {
    return new Promise((resolve) => {
      this.socket
        .once('sync/transmitted', () => {
          console.tron.log('SOCKETIO <--', 'sync/transmitted');
          resolve(true);
        });
    });
  }

  listenSync = () => {
    return new Promise((resolve) => {
      this.socket
        .on('sync-update', (userId) => {
          console.tron.log('SOCKETIO <--', 'sync-update', userId);
          resolve(userId);
        });
    });
  }

  sendSyncStatus = async (statusData) => {
    if (!this.isAuth) {
      let [err] = await to(this.auth());
      if (err) {
        throw err;
      }
    }

    this.socket.emit('sync-status', statusData);
    console.tron.log('SOCKETIO -->', 'sync-status', statusData);

    return new Promise((resolve) => {
      this.socket
        .once('sync-status/invalid-data', () => {
          console.tron.log('SOCKETIO <--', 'sync-status/invalid-data');
          resolve(new Error('invalid data'));
        })
        .once('sync-status/received', () => {
          console.tron.log('SOCKETIO <--', 'sync-status/received');
          resolve(true);
        });
    });
  }

  trackSyncStatus = () => {
    return new Promise((resolve) => {
      this.socket
        .once('sync-status/transmitted', () => {
          console.tron.log('SOCKETIO <--', 'sync-status/transmitted');
          resolve(true);
        });
    });
  }

  listenSyncStatus = () => {
    return new Promise((resolve) => {
      this.socket
        .on('sync-status/update', (user_status) => {
          console.tron.log('SOCKETIO <--', 'sync-status/update', user_status);
          resolve(user_status);
        });
    });
  }

  init = (userId) => {
    return this.auth(userId);
  }
}

export default new Socket({ host: 'https://whereappu-sos-socket.mybluemix.net', path: '/sos' });
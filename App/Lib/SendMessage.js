import SendSMS from 'react-native-sms-x';
import Location from '../Lib/Location';

export default class SendMessage {
  static send(contact) {
    const promise = new Promise((resolve) => {

      let locs = {};
      Location.getCurrentLocation()
        .then(location => {
          locs = { ...location.coords };
        })
        .catch((error) => {
          console.tron.log('An error ocurred getting your current location', error);
        })
        .finally(() => {

          const { you, friend, number } = contact;
          const currentLocation = `http://maps.google.com/?q=${locs.latitude},${locs.longitude}`
          const message = JSON.stringify(`Hi, ${you}, your friend with the cellphone number ${friend}, just requested help at ${currentLocation}`)

          SendSMS.send(
            123,
            `${number}`,
            message,
            (msg) => resolve(msg)
          );
        });
    });

    return promise;
  }
}

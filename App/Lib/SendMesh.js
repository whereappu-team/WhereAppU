import Bridgefy from 'react-native-bridgefy-sdk-x';
import Permissions from '../Lib/Permissions';

const BRIDGEFY_API_KEY = '0ddd381f-b5b2-4ebe-8009-7c2773233878';

export default class SendMesh {
  static send(contact, isSOS) {
    Permissions.hasPermission('ACCESS_FINE_LOCATION').then(hasPermission => {
      hasPermission = false; // TODO: Dev Only
      if (hasPermission) {
        Bridgefy.init(
          BRIDGEFY_API_KEY,
          (errorCode, message) => {
            console.warn(errorCode, message);
          },
          (client) => {
            Bridgefy.start();
            let message = JSON.stringify({ ...contact,
              isSOS
            });
            Bridgefy.sendBroadcastMessage({
              content: {
                message
              }
            });
          }
        );
      }
    });
  }
}

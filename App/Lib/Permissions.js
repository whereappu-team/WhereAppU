import { PermissionsAndroid } from 'react-native';
import { to } from './Async';

export async function requestPermission(permission) {
  try {
    const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS[permission], {
      title: 'WhereAppU needs your permission',
      message: `WhereAppU needs access to ${permission}`
    });
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
}

export async function hasPermission(permission) {
  return await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS[permission]);
}

export async function obtainPermission(permission) {
  return (await hasPermission(permission)) ? true : await requestPermission(permission);
}

export async function obtainPermissions(permissions) {
  let err, result;
  for (let i = 0; i < permissions.length; i++) {
    [err, result] = await to(obtainPermission(permissions[i]));
    if (err) {
      throw err;
    }
    if (!result) {
      return false;
    }
  }
  return true;
}

export default {
  requestPermission,
  hasPermission,
  obtainPermission,
  obtainPermissions
};

import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { withNavigation } from 'react-navigation';

// Styles
import styles from './LinkButtonStyles';

const LinkButton = ({action, text}) => {
  return (
    <TouchableOpacity
      accessible={ true }
      accessibilityLabel={ 'Tap me to ask for help!' }
      style={ styles.button }
      onPress={ action }
    >
      <Icon name="heartbeat" size={30} color="#fff" style={styles.buttonIcon}/>
      <Text style={styles.buttonText}>{ text }</Text>
    </TouchableOpacity>
  )
}
export default LinkButton;

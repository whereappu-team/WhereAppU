import React, { Component } from 'react';
import { Image, View, Text } from 'react-native';
import { Images } from '../../Themes';
import MapboxView from '../Map/MapboxView';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';


// Styles
import styles from './ContactDetailsStyles';
import { Colors } from '../../Themes';

const duration = (t1, t2 = new Date()) => {
    t1 = (t1 instanceof Date ? t1 : new Date(t1)).valueOf();
    t2 = (t2 instanceof Date ? t2 : new Date(t2)).valueOf();
    const interval = t2 - t1;
    const tots = {
        milliseconds: interval,
        seconds: Math.floor(interval / 1000),
        minutes: Math.floor(interval / (1000 * 60)),
        hours: Math.floor(interval / (1000 * 60 * 60)),
        days: Math.floor(interval / (1000 * 60 * 60 * 24))
    }
    return {
        milliseconds: tots.milliseconds - tots.seconds * 1000,
        seconds: tots.seconds - tots.minutes * 60,
        minutes: tots.minutes - tots.hours * 60,
        hours: tots.hours - tots.days * 24,
        days: tots.days
    }
}

const normalizeDuration = (d) => {
    return `${d.days > 0 ? `${d.days}d ` : ''}${d.hours > 0 ? `${d.hours}h` : ''}${d.minutes > 0 ? `${d.minutes}m` : ''}`;
}

const ContactDetails = ({ status, name, phone, lastUpdated, loc }) => (
  <LinearGradient
        colors={[Colors.gradPrimary, Colors.gradSecondary, Colors.gradPrimary]}
        locations={[0,0.5,1]}
        style={styles.mainContainer}
      >
    <View style={styles.containerHeader}>
      <Text style={styles.contactHeader}>CONTACTO EN EMERGENCIA</Text>
    </View>
    <View style={styles.containerContent}>
      {/* <View style={styles.fieldContainer}>
        <Text style={styles.labelField}>ESTADO:</Text>
        <Text style={styles.labelDataField}>{ status }</Text>
      </View> */}
      <View style={styles.userFieldContainer}>
        <View style={styles.left}>
          <Icon name="user" size={40} color={Colors.white}  style={styles.dataIcon}/>
          <View style={styles.info}>
            <Text style={styles.name}>{ name }</Text>
            <Text style={styles.number}>{ phone }</Text>
          </View>
        </View>
        <View style={styles.right}>
          <Icon name="exclamation-triangle" size={33} color={Colors.danger} />
        </View>
      </View>
      <View style={styles.fieldInfo}>
        <Icon name="clock-o" size={25} color={Colors.secondaryColor}  style={styles.dataIcon}/>
        <View>
          <Text style={styles.labelField}>Última actualización:</Text>
          <Text style={styles.dataField}>Hace { normalizeDuration(duration(lastUpdated)) }</Text>
        </View>
      </View>
      <View style={styles.fieldInfo}>
        <Icon name="map-marker" size={25} color={Colors.secondaryColor}  style={styles.dataIcon}/>
        <View>
          <Text style={styles.labelField}>Localización</Text>
          <Text style={styles.dataField}>Long: {loc.lng} Lat: {loc.lat}</Text>
        </View>
      </View>
    </View>
    <View style={styles.mapContainer}>
      <MapboxView
          loc={loc}
          zoom={16}
      />
    </View>
  </LinearGradient>
);

export default ContactDetails;

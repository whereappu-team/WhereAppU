import { StyleSheet } from 'react-native';
import { Colors, ApplicationStyles } from '../../Themes';

export default StyleSheet.create({
  ...ApplicationStyles.contact,

  mainContainer: {
    flex: 1,
    justifyContent: 'space-between'
  },
  containerHeader: {
    marginVertical: 15,
  },
  contactHeader: {
    fontSize: 25,
    color: Colors.orangePrimary,
    textAlign: 'center',
    width: '100%',
  },
  containerContent: {
    flex: 1,
  },
  userFieldContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 5,
    marginHorizontal: 10,
  },
  mapContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
    marginHorizontal: 10,
  },
  fieldInfo:{
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10
  },
  labelField:{
    color: Colors.white,
    fontSize: 18
  },
  dataField: {
    color: Colors.white,
    fontSize: 16
  },
  dataIcon:{
    width: 50,
    textAlign: 'center',

  }
});

import React, { Component } from 'react';
import { Image, View, Text, ListView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Colors } from '../../Themes/';
import Icon from 'react-native-vector-icons/FontAwesome';


// For empty lists
// import AlertMessage from '../Components/AlertMessage'

// Styles
import styles from './ContactsListStyle';

class ContactsList extends Component {
  constructor(props) {
    super(props);
    /************************************************************
     * STEP 1
     * This is an array of objects with the properties you desire
     * Usually this should come from Redux mapStateToProps
     *************************************************************/
    const dataObjects = props.contacts;

    /************************************************************
     * STEP 2
     * Teach datasource how to detect if rows are different
     * Make this function fast!  Perhaps something like:
     *   (r1, r2) => r1.id !== r2.id}
     *************************************************************/
    const rowHasChanged = (r1, r2) => r1 !== r2;

    // DataSource configured
    const ds = new ListView.DataSource({ rowHasChanged });

    // Datasource is always in state
    this.state = {
      dataSource: ds.cloneWithRows(dataObjects)
    };
  }

  handleOnPress = (givenName, middleName, familyName, phone, status, location) => {
    this.props.navigation.navigate('ContactView', {
      givenName: givenName,
      middleName: middleName,
      familyName: familyName,
      phone: phone,
      status: status,
      location: location
    });
  }

  /* ***********************************************************
  * STEP 3
  * `renderRow` function -How each cell/row should be rendered
  * It's our best practice to place a single component here:
  *
  * e.g.
    return <MyCustomCell title={rowData.title} description={rowData.description} />
  *************************************************************/
  renderRow = (rowData) => {
    return (
      <TouchableOpacity 
        style={styles.row}
        onPress={() => {
          this.handleOnPress(
            rowData.givenName, 
            rowData.middleName, 
            rowData.familyName,  
            rowData.phoneNumbers[0].number, 
            rowData.isSOS,
            rowData.location
            )
          }
        }
      >
      
        <View style={styles.left}>

          <Icon name="user" size={40} color={Colors.white} style={styles.userIcon}/>

          <View style={styles.info} >
            <Text 
              style={styles.name}
              accessible={true}
              accessibilityLabel={'Tap me to go to the contact'}
              >
                {
                  `${rowData.givenName || ''} ${rowData.middleName || ''} ${rowData.familyName || ''} `
                }
            </Text>
            <Text 
              style={styles.number}>
                {
                  `${rowData.phoneNumbers[0].number} `
                }
            </Text>
          </View>

        </View>

        <View style={styles.right}>
          {rowData.isSOS ? (
              <Icon name="exclamation-triangle" size={20} color={Colors.redPrimary} accessible={true}/>
            ) : rowData.isConfirmed ? (
              <Icon name="check" size={20} color={Colors.green} accessible={true}/>
            ) : (
              <Icon name="unlink" size={20} color={Colors.bloodOrange} accessible={true}/>
          )}
        </View>
      </TouchableOpacity>
    );

  }

  /* ***********************************************************
  * STEP 4
  * If your datasource is driven by Redux, you'll need to
  * reset it when new data arrives.
  * DO NOT! place `cloneWithRows` inside of render, since render
  * is called very often, and should remain fast!  Just replace
  * state's datasource on newProps.
  *
  * e.g.
    componentWillReceiveProps (newProps) {
      if (newProps.someData) {
        this.setState(prevState => ({
          dataSource: prevState.dataSource.cloneWithRows(newProps.someData)
        }))
      }
    }
  *************************************************************/

  // Used for friendly AlertMessage
  // returns true if the dataSource is empty
  noRowData() {
    return this.state.dataSource.getRowCount() === 0;
  }

  // Render a footer.
  renderFooter = () => {
    // return <Text> - Footer - </Text>;
  };

  render() {

    return (
      <ListView
        contentContainerStyle={styles.listContent}
        dataSource={this.state.dataSource}
        renderRow={this.renderRow}
        renderFooter={this.renderFooter}
        enableEmptySections
        pageSize={15}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // ...redux state to props here
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactsList);

// React
import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

// Libraries
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';

// Styles
import styles from './SendSOSButtonStyles';

const SendSOSButton = ({ action }) =>
  <TouchableOpacity onPress={action} >
    <LinearGradient
      colors={['#E5476D', '#FE5438']}
      start={{ x: 0.0, y: 0.25 }} end={{ x: 0.5, y: 1.0 }}
      locations={[0,1]}
      accessible={true}
      accessibilityLabel={'Tap me to ask for help!'}
      style={styles.alertButton}
    >
      <Icon name="exclamation-triangle" size={30} color="#fff" />
      <Text style={styles.alertButtonText}>
        SEND
        {'\n'}
        S.O.S
      </Text>
    </LinearGradient>
  </TouchableOpacity>;

export default SendSOSButton;
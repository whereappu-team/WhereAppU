import { StyleSheet } from 'react-native';
import { Colors, ApplicationStyles } from '../../Themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  alertButton:{
    width: 150,
    height: 150,
    borderRadius: 150,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    marginVertical: 20
  },
  alertButtonText:{
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    width: '100%',
    color: Colors.white
  }
});

import React from 'react';
import { View, TouchableOpacity } from 'react-native'; 
import Icon from 'react-native-vector-icons/FontAwesome';
import WauLogo from '../../Images/react-svg/wau-logo';
import { NavigationActions } from 'react-navigation';
import { connect } from  'react-redux';

//Styles
import styles from './HeaderStyles';
import { Colors } from '../../Themes';

const Header = ({navigateTo, menuState, toggle}) => {

    const handleNavigation = route => {
		navigateTo(route)
	}

    return (
        <View style={styles.head}>

            <TouchableOpacity onPress={() => {handleNavigation('DashboardView')}} >
                <WauLogo/>
            </TouchableOpacity>
            
            <TouchableOpacity onPress={() => toggle()} >
                {menuState ? <Icon name="close" size={30} color={Colors.primaryOrange} /> : <Icon name="bars" size={30} color={Colors.white} />}
            </TouchableOpacity>
        </View>
    )
   
}

const mapDispatchToProps = dispatch => ({
    navigateTo: route => dispatch(NavigationActions.navigate({ routeName: route }))
});

export default connect(null, mapDispatchToProps)(Header);
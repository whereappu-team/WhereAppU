import React, { Component } from 'react';
import { View } from 'react-native';
import Mapbox from '@mapbox/react-native-mapbox-gl';

import Location from '../../Lib/Location';

import styles from './MapboxViewStyles';

// should really put this in an .env file using 'react-native-config' to import it... meh
const MAPBOX_TOKEN = 'pk.eyJ1Ijoid2F1bWFwYm94IiwiYSI6ImNqbTBha3o5ZjE1bnkzb210YnVidjU2OWQifQ.BCwfATf9Z4e25FKgKK4lgQ';

Mapbox.setAccessToken(MAPBOX_TOKEN);

const MapboxView = ({ loc, zoom }) => (
  <View style={styles.container}>
    <Mapbox.MapView
      centerCoordinate={[loc.lng, loc.lat]}
      zoomLevel={zoom}
      styleURL={Mapbox.StyleURL.Dark}
      style={styles.map}
    />
  </View>
);

export default MapboxView;

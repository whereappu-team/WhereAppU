import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.contact,
  defaultText:{
    color: Colors.white,
  },
  selectedText:{
    color: Colors.black,
  },
  selected: {
    backgroundColor: Colors.steel
  }
})

//React
import React, { Component } from 'react';
import { TouchableHighlight, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

//Styles
import styles from './ContactItemStyles';
import { Colors } from '../../Themes/';

class ContactItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected
    };
  }

  render() {
    const { action, contact } = this.props;
    const { givenName, middleName, familyName, phoneNumbers } = contact;
    const { selected } = this.state;
    return (
      <TouchableHighlight
        onPress={() => {
          this.setState(prevState => ({ selected: !prevState.selected }));
          setTimeout(() => {
            action(contact);
          }, 500); // sorry about this... some other day we'll find proper way to do it
        }}
      >
        <View style={[styles.row , selected && styles.selected]}>
          <View style={styles.left}>
            <Icon name="user" size={40} color={ selected ? Colors.black : Colors.white}  style={styles.userIcon}/>
            <View style={styles.info}>
              <Text style={[styles.name, selected ? styles.selectedText : styles.defaultText]}>{`${givenName || ''} ${middleName || ''} ${familyName || ''} `}</Text>
              <Text style={[styles.number, selected ? styles.selectedText : styles.defaultText]}>{phoneNumbers && phoneNumbers[0].number}</Text>
            </View>
          </View>
          <View style={styles.right}>
            {
              selected ? <Icon name="remove" size={20} color={Colors.primaryOrange}/> : <Icon name="plus" size={20} color={Colors.green}/>
            }
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

export default ContactItem;

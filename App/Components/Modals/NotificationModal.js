// React
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

// Libraries
import Modal from "react-native-modal";

const NotificationModal = ({
  isVisible,
	visibilityAction,
	messageType
}) => {
	const closeTimeOut = () => {
		setTimeout(() => {
			visibilityAction(false)
		}, 3000);
	}
  return(
		<Modal
			isVisible={ isVisible }
			animationIn='slideInUp'
			animationOut='slideOutDown'
			onBackButtonPress={() => {visibilityAction(false)}}
			backdropOpacity={0}
			onBackdropPress={() => {visibilityAction(false)}}
			animationOutTiming={600}
			onModalShow={closeTimeOut}
			style={{ justifyContent: "flex-end", margin: 0 }}>
			<View style={{ backgroundColor: "white", padding: 20 }}>
				<Text>{messageType} completed</Text>
				<TouchableOpacity onPress={ () => {visibilityAction(false)} }>
					<Text>Accept</Text>
				</TouchableOpacity>
			</View>
		</Modal>
	)
}
export default NotificationModal
// React
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

// Libraries
import Modal from "react-native-modal";
import AndroidOpenSettings from 'react-native-android-open-settings'

//Styles
import styles from './SelectEmergencyModalStyles';

const meshContent = {
	title: "You're complety offline ",
	content: "No worries! we still have our TCP signal 'WhereAppU-WiFi' click 'Check wifi' to look for it",
	extraContent: "If you hit cancel we're going to still send your message via Mesh technology (low frequency) we got You! ;)"
}
const wifiContent = {
	title: "Where App U signal found",
	content: "",
  extraContent: "If you hit cancel we're going to still send your message via Mesh technology (low frequency) we got You! ;)"
}

const SelectEmergencyModal = ({
  isVisible,
	closeAction,
	type
}) => {

	const data = type === 'mesh' ? meshContent : wifiContent
  return(
		<View>
			<Modal
				isVisible={ isVisible }
				animationIn='slideInUp'
				animationOut='slideOutDown'
				>
				<View style={styles.modalContainer} >
					<Text style={styles.modalTitle}>{data.title}</Text>
					<Text style={styles.helpText}>
						{data.content}
					</Text>
					<Text style={styles.helpText}>
						{data.extraContent}
					</Text>
					<View style={styles.actionButtons}>
						<TouchableOpacity onPress={ closeAction } style={[styles.button , styles.cancelButton, styles.mediumButton]}>
							<Text style={styles.buttonLabel}>Close</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={ () => AndroidOpenSettings.wifiSettings() } style={[styles.button , styles.sendButton, styles.mediumButton]}>
							<Text style={styles.buttonLabel}>Check wifi!</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Modal>
		</View>
	)
}
export default SelectEmergencyModal
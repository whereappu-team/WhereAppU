// React
import React, { Component } from 'react';
import { Text, TouchableOpacity, View, TextInput, Alert } from 'react-native';

// Libraries
import Modal from "react-native-modal";
import Icon from 'react-native-vector-icons/FontAwesome';

// Styles
import styles from './SelectEmergencyModalStyles';

// @Giovanni usted es la cagada parce

export default class PhoneModal extends Component {
  constructor(props) {
    super(props);
    this.state = { text: '' }
  }

	handleSubmit = () => {
		if (this.state.text.length === 10) {
			this.props.submitAction(this.state.text)
		} else {
			Alert.alert('Please try again', 'Add only numbers and 10 characters');
		}

	}

  render() {
		return(
			<View>
				<Modal
					isVisible={ this.props.isVisible }
					animationIn='slideInUp'
					animationOut='slideOutDown'
					>
					<View style={styles.modalContainer} >
						<Text style={styles.modalTitle}>LET'S START WITH YOUR PHONE NUMBER</Text>
						<Icon name="phone" size={60} color="#fff" style={styles.phoneIcon}/>
						<TextInput
							style={styles.input}
							onChangeText={text => this.setState({ text })}
							value={this.state.text}
							keyboardType = 'numeric'
							maxLength={10}
							autoFocus = {true}
							underlineColorAndroid='rgba(0,0,0,0)'
							selectionColor='white'
							/>
						<TouchableOpacity onPress={ () => this.handleSubmit() } style={[styles.button , styles.sendButton, styles.largeButton]}>
							<Text style={styles.buttonLabel}>ADD</Text>
						</TouchableOpacity>
					</View>
				</Modal>
			</View>
		)
	}
}
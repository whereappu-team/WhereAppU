import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes';
// import { ApplicationStyles } from '../../Themes'

export default StyleSheet.create({
//   ...ApplicationStyles.screen,
    modalContainer: {
        flex: 1, 
        backgroundColor: Colors.gradPrimary,
        justifyContent: 'space-between'
    },
    modalTitle: {
        width: '100%',
        textAlign: 'center',
        fontSize: 22,
        paddingTop: 20,
        color: Colors.orangePrimary
    },
    optionsContainer:{
        width: '100%',
        padding: 20,
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap'
    },
    alertOption: {
        width: 100,
        height: 100,
        margin: 10,
        backgroundColor: Colors.primaryOrange,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    optionLabel: {
        color: Colors.white,
        fontSize: 14,
        padding: 5,
    },
    helpText: {
        width: '100%',
        textAlign: 'center',
        fontSize: 18,
        color: Colors.white,
        padding: 10
    },
    actionButtons:{
        flexDirection: 'row'
    },
    button:{
        height: 60,
        alignItems: 'center',
        justifyContent: 'center' 
    },
    mediumButton:{
        width: '50%',
    },
    largeButton:{
        width: '100%',
    },
    buttonLabel:{
        color: Colors.white,
        fontSize: 20
    },
    cancelButton: {
        backgroundColor: Colors.secondaryColor
    },
    sendButton: {
        backgroundColor: Colors.primaryOrange
    },
    phoneIcon:{
        alignSelf: 'center'
    },
    input: {
        color: 'white', 
        height: 60, 
        borderColor: 'gray', 
        borderWidth: 1,
        fontSize: 35,
        width: '90%',
        marginHorizontal: '5%',
        textAlign: 'center'
    }

});

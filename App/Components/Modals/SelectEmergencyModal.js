// React
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

// Libraries
import Modal from "react-native-modal";
import CountDown from 'react-native-countdown-component';
import Icon from 'react-native-vector-icons/FontAwesome';

//Styles
import styles from './SelectEmergencyModalStyles';


// @Giovanni no vaya a cambiar el valor de `name`, si lo envía en mayusculas o lo que sea no funciona!
// @Johan.... Bueno... :P


const alertsTypes = [
	{ name: "earthquake", img: "/ruta o variable de la imagen" },
	{ name: "fire", img: "/ruta o variable de la imagen" },
	{ name: "landslide", img: "/ruta o variable de la imagen" },
	{ name: "flood", img: "/ruta o variable de la imagen" }
]

const SelectEmergencyModal = ({
  isVisible,
	closeAction,
	mainAction,
	emergencyTypeAction,
	userData,
	removeEmergencyType,
	onSOSSent
}) => {

	const handleSendSOS = () => {
		mainAction(userData)
		onSOSSent() //Activate to send sms, comment it for debugging purposes
		closeAction()
	}
	const handleCancel = () => {
		removeEmergencyType()
		closeAction()
	}

  return(
		<View>
			<Modal
				isVisible={ isVisible }
				animationIn='slideInUp'
				animationOut='slideOutDown'
				>
				<View style={styles.modalContainer} >
					<Text style={styles.modalTitle}>WHAT'S THE EMERGENCY?</Text>
					<View style={styles.optionsContainer}>
						{
							alertsTypes.map(alertType =>
								<TouchableOpacity style={styles.alertOption} onPress={ () => { emergencyTypeAction(alertType.name) } }>
									<Text style={styles.optionLabel}>
										{ alertType.name }
									</Text>
									<Icon name={ alertType.name === userData.disaster_type ? 'check' : 'plus' } size={20} color="#fff"/> 
								</TouchableOpacity>
							)
						}
					</View>
					<Text style={styles.helpText}>
						If you do not choose an option, we will send a general alert in:
					</Text>
					<CountDown
						digitBgColor={'#fff'}
						digitTxtColor={'#FE5438'}
						timeToShow={['S']}
						until={10}
						onFinish={handleSendSOS}
						onPress={() => alert('hello')}
						size={25}
						labelS={'Seconds'}
					/>
					<View style={styles.actionButtons}>
						<TouchableOpacity onPress={ handleCancel } style={[styles.button , styles.cancelButton, styles.mediumButton]}>
							<Text style={styles.buttonLabel}>CANCEL</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={ handleSendSOS } style={[styles.button , styles.sendButton, styles.mediumButton]}>
							<Text style={styles.buttonLabel}>SEND NOW</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Modal>
		</View>
	)
}
export default SelectEmergencyModal
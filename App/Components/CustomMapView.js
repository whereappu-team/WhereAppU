import React, { Component } from 'react';
import { View } from 'react-native';
import Location from '../Lib/Location';
import Mapbox from '@mapbox/react-native-mapbox-gl';

import styles from './Styles/CustomMapViewStyles';

// should really put this in an .env file using 'react-native-config' to import it... meh
const MAPBOX_TOKEN = 'pk.eyJ1Ijoid2F1bWFwYm94IiwiYSI6ImNqbTBha3o5ZjE1bnkzb210YnVidjU2OWQifQ.BCwfATf9Z4e25FKgKK4lgQ';

Mapbox.setAccessToken(MAPBOX_TOKEN);

export default class CustomMapView extends Component {
  constructor(props) {
    super(props);
    Location.getCurrentLocation();
  }

  render() {
    return (
      <View style={styles.container}>
        <Mapbox.MapView
          centerCoordinate={[-74.063644, 4.624335]}
          zoomLevel={6}
          styleURL={Mapbox.StyleURL.Dark}
          style={styles.map}
        />
      </View>
    );
  }
}

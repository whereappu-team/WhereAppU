import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes/'

export default StyleSheet.create({
  menuContainer:{
    flex: 1,
    backgroundColor: Colors.gradPrimary,
  },
  menuHeader: {
    backgroundColor: 'rgba(255,255,255,0.2)',
    paddingVertical: 20,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  menuHeaderText: {
    color: Colors.white,
    fontSize: 22
  },
  menuItem: {
      paddingVertical: 20,
      paddingHorizontal: 10,
      flexDirection: 'row',
      alignItems: 'center',
      borderBottomWidth: 1,
      borderColor: 'rgba(255,255,255,0.2)',
      justifyContent: 'space-between'
  },
  menuItemText:{
      color: Colors.white,
      fontSize: 20
  },
  menuItemIcon:{
    paddingHorizontal: 10
  }
})

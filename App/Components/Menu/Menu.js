import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { connect } from  'react-redux';

//Styles
import styles from './MenuStyle';
import { Colors } from '../../Themes';


const Menu = ({ navigateTo, toggle }) => {

	const handleNavigation = route => {
		navigateTo(route)
		setTimeout(() => {
			toggle()
		}, 500);
	}

	return (
		<View style={styles.menuContainer}>
			<View style={styles.menuHeader}>
				<Text style={styles.menuHeaderText} >MENU</Text>
				<Icon name="bars" size={20} color={Colors.white} style={styles.menuItemIcon}/>
			</View>
			<TouchableOpacity style={styles.menuItem}>
				<Text style={styles.menuItemText}>User profile</Text>
				<Icon name="user" size={20} color={Colors.white} style={styles.menuItemIcon}/>
			</TouchableOpacity>
			<TouchableOpacity style={styles.menuItem} onPress={() => { handleNavigation('AddNewContact') }}>
				<Text style={styles.menuItemText}>Edit contacts</Text>
				<Icon name="pencil" size={20} color={Colors.white} style={styles.menuItemIcon}/>
			</TouchableOpacity>
			<TouchableOpacity style={styles.menuItem}>
				<Text style={styles.menuItemText}>Info</Text>
				<Icon name="info-circle" size={20} color={Colors.white} style={styles.menuItemIcon}/>
			</TouchableOpacity>
			<TouchableOpacity style={styles.menuItem}>
				<Text style={styles.menuItemText}>Help</Text>
				<Icon name="life-saver" size={20} color={Colors.white} style={styles.menuItemIcon}/>	
			</TouchableOpacity>
		</View>
	)
}
const mapDispatchToProps = dispatch => ({
  navigateTo: route => dispatch(NavigationActions.navigate({ routeName: route }))
});

export default connect(null, mapDispatchToProps)(Menu);

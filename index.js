import './App/Config/ReactotronConfig'
import { AppRegistry, YellowBox } from 'react-native'
import App from './App/Containers/App'

console.disableYellowBox = true;

AppRegistry.registerComponent('WhereAppU', () => App)

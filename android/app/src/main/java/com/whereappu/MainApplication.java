package com.whereappu;

import android.app.Application;

import com.BV.LinearGradient.LinearGradientPackage;
import com.horcrux.svg.SvgPackage;
import com.facebook.react.ReactApplication;
import com.levelasquez.androidopensettings.AndroidOpenSettingsPackage;
import com.devstepbcn.wifi.AndroidWifiPackage;
import com.peel.react.TcpSocketsModule;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import com.butchmarshall.reactnative.google.nearby.connection.NearbyConnectionPackage;
import com.jamesisaac.rnbackgroundtask.BackgroundTaskPackage;
import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import com.i18n.reactnativei18n.ReactNativeI18n;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import com.someone.sendsms.SendSMSPackage;
import com.mapbox.rctmgl.RCTMGLPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
        new MainReactPackage(),
            new AndroidOpenSettingsPackage(),
        new AndroidWifiPackage(),
        new TcpSocketsModule(),
        new ReactNativePushNotificationPackage(),
        new NearbyConnectionPackage(),
        new BackgroundTaskPackage(),
        new ReactNativeContacts(),
        new ReactNativeI18n(),
        new RNDeviceInfo(),
        new ReactNativeConfigPackage(),
        new SendSMSPackage(),
        new RCTMGLPackage(),
        new LinearGradientPackage(),
        new SvgPackage(),
        new RNFusedLocationPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);

    BackgroundTaskPackage.useContext(this);
  }
}
